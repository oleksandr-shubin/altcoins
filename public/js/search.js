new Vue({
    el:'#search',
    data: {
        exchanges : [],
        currencies: [],
        matchExchanges: [],
        matchCurrencies: [],
        needle    : '',
        isVisible : false,
        limit:6,
    },

    watch: {
        needle: function(val, oldVal) {

            if (val === "") {
                this.isVisible = false;
                return;
            }

            this.matchExchanges = [];
            this.matchCurrencies = [];

            this.exchanges.every(function(exchange) {
                if(exchange.name.toLowerCase().indexOf(val.toLowerCase()) !== -1){
                    this.matchExchanges.push(exchange);
                    if (this.matchExchanges.length >= this.limit) {
                        return false;
                    }
                }
                return true;
            }.bind(this));

            this.currencies.every(function(currency) {
                if(currency.name.toLowerCase().indexOf(val.toLowerCase()) !== -1){
                    this.matchCurrencies.push(currency);
                    if (this.matchCurrencies.length >= this.limit) {
                        return false;
                    }
                }
                return true;
            }.bind(this));

            if(this.matchExchanges.length > 0 || this.matchCurrencies.length > 0) {
                this.isVisible = true;
            } else {
                this.isVisible = false;
            }
        }
    },

    methods: {
        findExchanges: function() {
            var url = "/search/exchanges";

            axios.get(url)
                .then(function(response){
                    this.exchanges = response.data;
                }.bind(this))
                .catch(function(error) {});
        },

        findCurrencies: function() {
            var url = "/search/currencies";

            axios.get(url)
                .then(function(response){
                    this.currencies = response.data;
                }.bind(this))
                .catch(function(error) {});
        },

        formatPrice: function(value, afterComma) {
            if (value === null) {
                return '?';
            }
            return '$' + parseFloat(value).toFixed(afterComma).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        },
    },

    mounted: function() {
        this.exchanges = this.findExchanges();
        this.currencies = this.findCurrencies();
    }
});