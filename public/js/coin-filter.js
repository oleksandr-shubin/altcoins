new Vue({
    el:"#fullTable",
    data: {
        currencies: [],
        filteredCurrencies: [],
        marketCapFilterValue: 0,
        priceFilterValue: 0,
        volumeFilterValue: 0,
        marketCapFilters: [
            {
                min: 0,
                max: Number.POSITIVE_INFINITY,
            },
            {
                min: 1000000000,
                max: Number.POSITIVE_INFINITY,
            },
            {
                min: 100000000,
                max: 1000000000,
            },
            {
                min: 10000000,
                max: 100000000,
            },
            {
                min: 1000000,
                max: 10000000,
            },
            {
                min: 100000,
                max: 1000000,
            },
            {
                min: 0,
                max: 100000,
            }
        ],
        priceFilters: [
            {
                min: 0,
                max: Number.POSITIVE_INFINITY,
            },
            {
                min: 100,
                max: Number.POSITIVE_INFINITY,
            },
            {
                min: 1,
                max: 100,
            },
            {
                min: 0.01,
                max: 1,
            },
            {
                min: 0.0001,
                max: 0.01,
            },
            {
                min: 0,
                max: 0.0001
            }

        ],
        volumeFilters: [
            {
                min:0,
                max: Number.POSITIVE_INFINITY,
            },
            {
                min: 10000000,
                max: Number.POSITIVE_INFINITY,
            },
            {
                min: 1000000,
                max: Number.POSITIVE_INFINITY,
            },
            {
                min: 100000,
                max: Number.POSITIVE_INFINITY,
            },
            {
                min: 10000,
                max: Number.POSITIVE_INFINITY,
            },
            {
                min: 1000,
                max: Number.POSITIVE_INFINITY,
            },
        ]
    },

    watch: {
        marketCapFilterValue: function(value) {
            this.filterCurrencies(value);
        },
        priceFilterValue: function(value) {
            this.filterCurrencies(value)
        },
        volumeFilterValue: function(value) {
            this.filterCurrencies(value)
        },
    },

    methods: {
        filterCurrencies: function(value) {
            this.filteredCurrencies = this.inRangeFilter(this.currencies, "market_cap_usd", this.marketCapFilters[this.marketCapFilterValue]);
            this.filteredCurrencies = this.inRangeFilter(this.filteredCurrencies, "price_usd", this.priceFilters[this.priceFilterValue]);
            this.filteredCurrencies = this.inRangeFilter(this.filteredCurrencies, "volume_usd_24h", this.volumeFilters[this.volumeFilterValue]);
        },

        inRangeFilter: function(array, filterKey, filter) {
            return array.filter(function(item){
                if (item[filterKey] >= filter.min && item[filterKey] <= filter.max) {
                    return true;
                } else {
                    return false;
                }
            })
        },

        fillTable: function() {
            var url = window.location.href;

            axios.get(url)
                .then(function(response){
                    this.currencies = response.data;
                    this.filteredCurrencies = this.currencies;
                }.bind(this))
                .catch(function(error){
                });
        },

        formatPrice: function(value, afterComma) {
            if (value === null) {
                return '?';
            }
            return '$' + parseFloat(value).toFixed(afterComma).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        },

        formatIntegerPrice: function(value) {
            if (value === null) {
                return '?';
            }
            return '$' + parseInt(value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        },

        formatPercent: function(value) {
            if (value === null) {
                return '?';
            }

            return value + '%';
        },

        detectClass: function(value) {
            if (value === 0 || value === null) {
                return "";
            } else if (value > 0){
                return "green";
            } else {
                return "red"
            }
        },
    },

    mounted: function() {
        this.fillTable();
    }
});