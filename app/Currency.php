<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class Currency extends Model
{
    use MassInsertOrUpdatable, RelationsDetachable;

    protected $fillable = array(
        'name',
        'icon_link',
        'is_token',
        'coinmarketcap_id',
        'max_supply',
        'burned_supply',
        'private_supply',
        'has_available_supply_on_coinmarketcap',
        'available_supply_link',
        'available_supply_index'
    );

    public function symbols() {
        return $this->hasMany(Symbol::class, 'currency_id');
    }

    public function mainSymbol() {
        return $this->hasOne(Symbol::class, 'currency_id');
    }

    public function links() {
        return $this->hasMany(Link::class, "currency_id");
    }

//    -------------------- MARKETS ---------------------------------------------------------------

    public function totalMarketsVolume() {
        return DB::table('markets')
            ->select(DB::raw("SUM(volume_usd_24h) as total_volume"))
            ->where('base_id', '=', $this->id)
            ->orWhere('quote_id', '=', $this->id)
            ->first()->total_volume;
    }

    public function markets() {
        return $this->marketsWhereIsBase->merge($this->marketsWhereIsQuote);
    }

    public function topMarketsSortedByVolumeUsd24hDesc() {
        return $this->topMarketsWhereIsBase
            ->merge($this->topMarketsWhereIsQuote)
            ->sortByDesc('volume_usd_24h')
            ->take(10);
    }

    public function topMarketsWhereIsBase() {
        return $this->marketsWhereIsBase()->orderByDesc('volume_usd_24h')->nPerGroup('base_id', 10);
    }

    public function topMarketsWhereIsQuote() {
        return $this->marketsWhereIsQuote()->orderByDesc('volume_usd_24h')->nPerGroup('quote_id', 10);
    }

    public function marketsSortedByVolumeUsd24hDesc() {
        return $this->markets()->sortByDesc('volume_usd_24h');
    }

    public function marketsWhereIsBase() {
        return $this->hasMany(Market::class, 'base_id');
    }

    public function marketsWhereIsQuote() {
        return $this->hasMany(Market::class, 'quote_id');
    }

    public function marketsWhereIsBaseAndHasFees() {
        return $this->marketsWhereIsBase()->where('has_fees', '=', true);
    }

    public function marketsWhereIsQuoteAndHasFees() {
        return $this->marketsWhereIsQuote()->where('has_fees', '=', true);
    }

    public function marketsWhereIsQuoteAgainstBitcoin() {
        return $this->marketsWhereIsQuote()->where("base_id", "=", function($query) {
            $query->select("id")->from("currencies")->where("name", "=", "Bitcoin");
        });
    }

//    --------------------------------- Historical data --------------------------------------------------------

    public function historicalDatas() {
        return $this->hasMany(HistoricalData::class, 'currency_id');
    }

    public function historicalDatasSortedByDateDesc() {
        return $this->historicalDatas()->orderByDesc('date')->get();
    }

    public function historicalDatasSortedByDateAsc() {
        return $this->historicalDatas()->orderBy('date')->get();
    }

    public function historicalDataLastDay() {
        return $this->historicalDatas->where('date', '>=', Carbon::now()->subDays(2));
    }

    public function historicalDataLastWeek() {
        return $this->historicalDatas->where('date', '>=', Carbon::now()->subWeek());
    }

    public function historicalDataLastMonth() {
        return $this->historicalDatas->where('date', '>=', Carbon::now()->subMonth());
    }

    public function priceLast7Days() {
        return implode(', ', $this->tickersLastWeek()->pluck('price_usd')->toArray());

    }

//    ------------------------------ Ticker --------------------------------------------------------

    public function tickers() {
        return $this->hasMany(Ticker::class, 'currency_id');
    }

    public function ticker() {
        return $this->hasOne(Ticker::class, 'currency_id');
    }

//   ---------------------------- Needed for eager loading with constraint --------------------------
    public function tickerLastHour() {
        return $this->ticker();
    }

//    Needed for eager loading with constraint
    public function tickerLastDay() {
        return $this->ticker();
    }

    //    Needed for eager loading with constraint
    public function tickerLastWeek() {
        return $this->ticker();
    }

//    --------------------------- end eager loading -------------------------------------------------

    public function tickersLastDay() {
        return $this->tickers()->where('created_at', '>=', Carbon::now()->subDay());
    }

    public function tickersLastWeek() {
        return $this->tickers()->where('created_at', '>=', Carbon::now()->subWeek());
    }

//    ------------------------------- Volume -------------------------------------------------------

    public function volumeDay() {
        return $this->historicalDataLastDay()->sum('volume_usd');
    }

    public function volumeWeek() {
        return $this->historicalDataLastWeek()->sum('volume_usd');
    }

    public function volumeMonth() {
        return $this->historicalDataLastMonth()->sum('volume_usd');
    }

    public function volumePercent($totalVolume) {
        if ($totalVolume == null || $totalVolume == 0) {
            return null;
        } else {
            return $this->volume_usd_24h / $totalVolume * 100;
        }
    }
}
