<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Symbol extends Model
{

    protected $guarded = [];

    public function currency() {
        return $this->belongsTo(Currency::class, 'currency_id');
    }
}
