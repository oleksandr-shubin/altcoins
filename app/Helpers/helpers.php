<?php

if (! function_exists('nullable_money_format')) {
    function nullable_money_format($number, $decimals = 0) {
        if (is_null($number)) {
            return "?";
        } else {
            return "$" . number_format($number, $decimals);
        }
    }
}

if (!function_exists('nullable_percent_format')) {
    function nullable_percent_format($percent) {
        if (is_null($percent)) {
            return "?";
        } else {
            return number_format($percent, 2) . "%";
        }
    }
}

if (!function_exists('nullable_number_format')) {
    function nullable_number_format($number, $decimals = 0) {
        if (is_null($number)) {
            return "?";
        } else {
            return number_format($number, $decimals);
        }
    }
}

if (!function_exists('is_active')) {
    function is_active($routeName, $output = "active") {
        if (Route::currentRouteName() == $routeName) {
            return $output;
        }
    }
}