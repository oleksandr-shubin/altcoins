<?php


namespace App\Repository;


class LinkEloquentRepository
{
    public function findAllByCurrency($currency)
    {
        return $currency->links;
    }

    public function delete($link)
    {
        $link->delete();
    }
}