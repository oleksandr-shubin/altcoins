<?php


namespace App\Repository;

use App\Exchange;
use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Promise\EachPromise;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\DomCrawler\Crawler;

class ExchangeCoinmarketcapRepository
{
    public function findAll() {
        $uri = "https://coinmarketcap.com/exchanges/volume/24-hour/all/";
        $client = new Client();
        $crawler = $client->request('GET', $uri);
        $tableBodyCrawler = $crawler->filter('table');
        $rows = $tableBodyCrawler->children();

        $exchanges = array();
        foreach ($rows as $row) {
            if ($this->hasId($row)) {
                $anchor = $row->getElementsByTagName('a')[0];
                $exchange = array();
                $exchange['name'] = $anchor->textContent;
                $exchange['coinmarketcap_id'] = explode('/', $anchor->getAttribute('href'))[2];
                $exchange['has_api'] = false;
                $exchanges[] = $exchange;
            }
        }
        $exchanges = $this->updateExchangesSetWebsite($exchanges);
        return $exchanges;
    }

    private function hasId($row) {
        if ($row->getAttribute('id') === "") {
            return false;
        }

        return true;
    }

    private function updateExchangesSetWebsite($exchanges) {
        $baseUri = "https://coinmarketcap.com/exchanges";

        $client = new GuzzleClient();
        $promises = array();


        for ($i = 0; $i < count($exchanges); $i++) {
            $promises[$i] = $client->getAsync($baseUri. "/" . $exchanges[$i]['coinmarketcap_id']);
        }

        $eachPromise = new EachPromise($promises, [
            'fulfilled' => function (ResponseInterface $response, $index) use (&$exchanges) {
                $crawler = new Crawler($response->getBody()->getContents());
                $ulCrawler = $crawler->filter('.col-xs-12 > .list-unstyled');
                $aCrawler = $ulCrawler->filter("a")->first();
                $link = $aCrawler->getNode(0)->getAttribute('href');
                $exchanges[$index]['link'] = $link;
            },
            'rejected' => function($reason, $index) use ($exchanges) {
                dump("updateExchangesSetWebsite:rejected: " . $exchanges['index']['name']);
            }
        ]);

        $eachPromise->promise()->wait();

        return $exchanges;
    }
}