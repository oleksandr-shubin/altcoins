<?php


namespace App\Repository;


use App\Currency;
use App\Link;
use App\Symbol;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class CurrencyEloquentRepository
{
    public function find($id) {
        return Currency::where("id", "=", $id)->first();
    }

    public function findByName($name) {
        return Currency::where("name", "=", $name)->first();
    }

    public function findAll() {
        return Currency::all();
    }

    public function findAllCryptoCurrencies() {
        return Currency::where('is_fiat', '=', false)->get();
    }

    public function findAllCryptoCurrenciesWithMainSymbolSortByMarketCap() {
        return Currency::with('mainSymbol')
            ->where('is_fiat', '=', false)
            ->orderBy("market_cap_usd", "desc")
            ->get();
    }

    public function findAllCryptoCurrenciesWithMarkets() {
        $currencies = Currency::with('marketsWhereIsQuote', 'marketsWhereIsBase')
            ->where('is_fiat', '=', false)
            ->get();
        $currencies = $currencies->map(function($currency, $key){
            $currency->markets = $currency->marketsWhereIsBase->merge($currency->marketsWhereIsQuote);
            unset($currency->marketsWhereIsBase);
            unset($currency->marketsWhereIsQuote);
            return $currency;
        });
        return $currencies;
    }

    public function findAllCryptoCurrenciesWithMarketsWhereHasFees() {
        $currencies = Currency::with('marketsWhereIsQuoteAndHasFees', 'marketsWhereIsBaseAndHasFees')
            ->where('is_fiat', '=', false)
            ->get();
        $currencies = $currencies->map(function($currency, $key){
            $currency->markets = $currency->marketsWhereIsBaseAndHasFees->merge($currency->marketsWhereIsQuoteAndHasFees);
            unset($currency->marketsWhereIsBaseAndHasFees);
            unset($currency->marketsWhereIsQuoteAndHasFees);
            return $currency;
        });
        return $currencies;
    }

    public function findAllCryptoCurrenciesWithThreeTickers() {
        return Currency::with([
            'tickerLastHour' => function($query) {
                $query->where('created_at', '>=', Carbon::now()->subHour())
                    ->orderBy('created_at', 'asc')->nPerGroup('currency_id', 1);
            },
            'tickerLastDay' => function($query) {
                $query->where('created_at', '>=', Carbon::now()->subDay())
                    ->orderBy('created_at', 'asc')->nPerGroup('currency_id', 1);
            },
            'tickerLastWeek' => function($query) {
                $query->where('created_at', '>=', Carbon::now()->subWeek())
                    ->orderBy('created_at', 'asc')->nPerGroup('currency_id', 1);
            },
        ])->where('is_fiat', '=', false)->get();
    }

    public function findAllCryptoCurrenciesWithLastDayTickers() {
        return Currency::with('tickersLastDay')->where('is_fiat', '=', false)->get();
    }

    public function findAllCryptoCurrenciesWithMarketsWhereIsBase() {
        return Currency::with('marketsWhereIsBase')
            ->where('is_fiat', '=', false)
            ->get();
    }

    public function findPaginatedCryptoCurrenciesSortByMarketCap($perPage) {
        return Currency::where('is_fiat', '=', false)->orderBy("market_cap_usd", "desc")->simplePaginate($perPage);
    }

    public function findPaginatedCryptoCurrenciesLikeNameSortByMarketCap($perPage, $name) {
        return Currency::where('is_fiat', '=', false)
            ->where('name', 'LIKE', "%$name%")
            ->orderBy("market_cap_usd", "desc")
            ->simplePaginate($perPage);
    }

    public function findAllCoinmarketcapCryptoCurrencies() {
        return Currency::where('is_fiat', '=', false)->whereNotNull('coinmarketcap_id')->get();
    }

    public function findAllTokens() {
        return Currency::where('is_token', '=', true)->get();
    }

    public function findAllTokensWithMainSymbolSortByMarketCap() {
        return Currency::with('mainSymbol')
            ->where('is_token', '=', true)
            ->orderBy("market_cap_usd", "desc")
            ->get();
    }

    public function findPaginatedTokensSortByMarketCap($perPage) {
        return Currency::where('is_token', '=', true)->orderBy("market_cap_usd", "desc")->simplePaginate($perPage);
    }

    public function findAllCryptoCurrenciesWithTopMarkets() {
         return Currency::with(
             'topMarketsWhereIsBase.exchange',
             'topMarketsWhereIsQuote.exchange',
            'topMarketsWhereIsBase.baseCurrency',
            'topMarketsWhereIsBase.quoteCurrency',
            'topMarketsWhereIsQuote.baseCurrency',
            'topMarketsWhereIsQuote.quoteCurrency'
         )
             ->where('is_fiat', '=', false)->get();
    }

    public function findAllCryptoCurrenciesWithMonthlyHistoricalData() {
        return Currency::with(['historicalDatas' => function($query) {
            $query->where('date', '>=', Carbon::now()->subMonth());
        }])->where('is_fiat', '=', false)->get();
    }

    public function findPaginatedCryptoCurrenciesWithMonthlyHistoricalData($perPage) {
        return Currency::with(['historicalDatas' => function($query) {
            $query->where('date', '>=', Carbon::now()->subMonth());
        }])->where('is_fiat', '=', false)->simplePaginate($perPage);
    }

    public function findAllCoins() {
        return Currency::where([
            ['is_token', '=', false],
            ['is_fiat', '=', false],
        ])->get();
    }

    public function findAllCoinsWithMainSymbolSortByMarketCap() {
        return Currency::with('mainSymbol')
            ->where([
                ['is_token', '=', false],
                ['is_fiat', '=', false],
            ])
            ->orderBy("market_cap_usd", "desc")
            ->get();
    }

    public function findPaginatedCoinsSortByMarketCap($perPage) {
        return Currency::where([
            ['is_token', '=', false],
            ['is_fiat', '=', false],
        ])->orderBy("market_cap_usd", "desc")->simplePaginate($perPage);
    }

    public function findAllFiatCurrencies() {
        return Currency::where('is_fiat', '=', true)->get();
    }

    public function save($currency) {
        $currency->save();
    }

    public function saveCurrenciesAndMainSymbols($currenciesDataToSave) {
        $savedCurrencies = collect();
        foreach ($currenciesDataToSave as $currencyDataToSave) {
            DB::transaction(function() use ($currencyDataToSave, $savedCurrencies) {
                $currency = new Currency();
                $currency->name = $currencyDataToSave['name'];
                $currency->is_token = $currencyDataToSave['is_token'];
                $currency->is_fiat = $currencyDataToSave['is_fiat'];
                if ($currency->is_fiat == false) {
                    $currency->coinmarketcap_id = $currencyDataToSave['coinmarketcap_id'];
                    $currency->price_usd = $currencyDataToSave['price_usd'];
                    $currency->price_btc = $currencyDataToSave['price_btc'];
                    $currency->volume_usd_24h = $currencyDataToSave['volume_usd_24h'];
                    $currency->market_cap_usd = $currencyDataToSave['market_cap_usd'];
                    $currency->available_supply = $currencyDataToSave['available_supply'];
                    $currency->total_supply = $currencyDataToSave['total_supply'];
                    $currency->max_supply = $currencyDataToSave['max_supply'];
                    $currency->percent_change_1h = $currencyDataToSave['percent_change_1h'];
                    $currency->percent_change_24h = $currencyDataToSave['percent_change_24h'];
                    $currency->percent_change_7d = $currencyDataToSave['percent_change_7d'];
                    $currency->last_updated_on_coinmarketcap = $currencyDataToSave['last_updated_on_coinmarketcap'];
                    $currency->has_available_supply_on_coinmarketcap = $currencyDataToSave['has_available_supply_on_coinmarketcap'];
                }
                $currency->save();
                $savedCurrencies->push($currency);

                $symbol = new Symbol();
                $symbol->value = $currencyDataToSave['symbol'];
                $symbol->currency_id = $currency->id;
                $symbol->save();
            });
        }
        return $savedCurrencies;
    }

    public function saveLinks($links) {
        foreach (array_chunk($links->toArray(), 1000) as $linksChunk) {
            Link::insert($linksChunk);
        }
    }

//  FIXME
//  Doesn't take into account those currencies, really affected, asumes ALL coinmarketcap currencies
    public function updateCoinmarketcapCryptoCurrenciesSetIconLinks($cryptoCurrencies, $paths) {
        $keyedCryptoCurrencies = $cryptoCurrencies->keyBy('coinmarketcap_id');
        $currenciesToUpdate = collect();
        foreach ($paths as $currencyCoinmarketcapId => $icon_link) {
            $currency = $keyedCryptoCurrencies->get($currencyCoinmarketcapId);
            $currency->icon_link = $icon_link;
            $currenciesToUpdate->push($currency);
        }
        $this->updateBatch($currenciesToUpdate);
    }

    public function updateBatch($currencies) {
        $currenciesToUpdate = $currencies->map(function($currency, $key) {
            $currency->detachAllRelations();
            return $currency;
        });
        foreach(array_chunk($currenciesToUpdate->toArray(), 1000) as $currenciesChunk) {
            Currency::insertOrUpdate($currenciesChunk);
        }
    }

    public function delete($currency) {
        $currency->delete();
    }
}