<?php


namespace App\Repository;


use App\HistoricalData;
use App\Utility\CoinmarketcapDataFilter;
use DOMDocument;
use GuzzleHttp\Pool;
use GuzzleHttp\Promise\EachPromise;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Carbon;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Client as GuzzleClient;
use Symfony\Component\DomCrawler\Crawler;

class HistoricalDataCoinmarketcapRepository
{

    const COINMARKETCAP_START_DATE = 20130428;

    public function findAll($cryptoCurrencies) {
        $maxExecutionTime = ini_get('max_execution_time');
        ini_set('max_execution_time', 900);

        $historicalDatas = collect();

        $baseUri = "https://coinmarketcap.com/currencies/";
        $endDate = Carbon::now()->format("Ymd");
        $endUri = "/historical-data/?start=" . self::COINMARKETCAP_START_DATE . "&end=" . $endDate;


        $client = new GuzzleClient([
            'headers' => [
                'Connection' => 'close'
            ],
        ]);

        $count = $cryptoCurrencies->count();

        $requests = function ($cryptoCurrencies) use ($baseUri, $endUri, $count ) {
            for ($i = 0; $i < $count; $i++) {
                $uri = $baseUri . $cryptoCurrencies[$i]->coinmarketcap_id . $endUri;
                yield new Request('GET', $uri);
            }
        };

        $pool = new Pool($client, $requests($cryptoCurrencies), [
//            'concurrency' => 1,
            'fulfilled' => function ($response, $index) use ($cryptoCurrencies, &$historicalDatas) {
                $before = microtime(true);

                $content =  $response->getBody()->getContents();
                $newHistoricalDatas = $this->parseHistoricalDatas($content, $cryptoCurrencies[$index]);
                $historicalDatas = $historicalDatas->merge($newHistoricalDatas);
                $response->getBody()->close();

            },
            'rejected' => function ($reason, $index) use ($cryptoCurrencies) {
                dump("findAll:rejected: " . $cryptoCurrencies[$index]->coinmarketcap_id);
            },
        ]);

        // Initiate the transfers and create a promise
        $promise = $pool->promise();

        // Force the pool of requests to complete.
        $promise->wait();

        ini_set('max_execution_time', $maxExecutionTime);
        return $historicalDatas;
    }

    private function parseHistoricalDatas($content, $cryptoCurrency) {
        $historicalDatas = collect();

        $crawler = new Crawler($content);
        $tableBodyCrawler = $crawler->filter('table > tbody');
        $rows = $tableBodyCrawler->children();
        foreach($rows as $row) {
            $historicalData = new HistoricalData();
            $date = $row->getElementsByTagName('td')[0]->textContent;
            $historicalData->date = Carbon::parse($date)->toDateTimeString();
            $historicalData->open_usd   = CoinmarketcapDataFilter::filter($row->getElementsByTagName('td')[1]->getAttribute('data-format-value'));
            $historicalData->high_usd   = CoinmarketcapDataFilter::filter($row->getElementsByTagName('td')[2]->getAttribute('data-format-value'));
            $historicalData->low_usd    = CoinmarketcapDataFilter::filter($row->getElementsByTagName('td')[3]->getAttribute('data-format-value'));
            $historicalData->close_usd  = CoinmarketcapDataFilter::filter($row->getElementsByTagName('td')[4]->getAttribute('data-format-value'));
            $historicalData->volume_usd = CoinmarketcapDataFilter::filter($row->getElementsByTagName('td')[5]->getAttribute('data-format-value'));
            $historicalData->market_cap_usd = CoinmarketcapDataFilter::filter($row->getElementsByTagName('td')[6]->getAttribute('data-format-value'));
            $historicalData->currency_id = $cryptoCurrency->id;
            $historicalDatas->push($historicalData);
        }
        return $historicalDatas;
    }
}