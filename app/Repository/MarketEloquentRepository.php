<?php


namespace App\Repository;


use App\Currency;
use App\Market;
use App\Pair;
use App\Symbol;
use App\Utility\CoinmarketcapDataFilter;
use App\Utility\ModelTool;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class marketEloquentRepository
{

    const BITCOIN_CURRENCY_NAME = "Bitcoin";
    const USD_CURRENCY_NAME = "USD";

    public function findAll() {
        return Market::all();
    }

    public function findAllWithQuoteCurrencies() {
        return Market::with("quoteCurrency")->get();
    }

    public function findAllByExchanges($exchanges) {
        $exchangesIds = $exchanges->pluck('id')->all();
        return Market::whereIn('exchange_id', $exchangesIds)->get();
    }

    public function findPaginatedMarketsByExchangeSortByVolume($perPage, $exchange) {
        return Market::with('baseCurrency', 'quoteCurrency')
            ->where('exchange_id', $exchange->id)
            ->orderByDesc("volume_usd_24h")
            ->simplePaginate($perPage);
    }

    public function findAllMarketsWithCurrenciesByCurrencyIdSortedByVolumeUsd24hDesc($currencyId) {
        return Market::with("baseCurrency", "quoteCurrency")
            ->where("base_id", "=", $currencyId)
            ->orWhere("quote_id", "=", $currencyId)
            ->orderByDesc("volume_usd_24h")
            ->get();
    }

    public function findBitcoinUsdMarkets() {
        return Market::where("base_id", "=", function($query) {
            $query->select("id")->from("currencies")->where("name", "=", self::BITCOIN_CURRENCY_NAME);
        })->where("quote_id", "=", function($query) {
            $query->select("id")->from("currencies")->where("name", "=", self::USD_CURRENCY_NAME);
        })->get();
    }

    public function saveMarkets($marketsDataFromCoinmarketcap) {
        $marketsToSave = array();
        foreach($marketsDataFromCoinmarketcap as $exchangeId => $marketsData) {
            foreach ($marketsData as $marketData) {
                $baseCurrency = Currency::where('name', '=', $marketData['baseCurrencyName'])->first();
                $quoteCurrencySymbol = Symbol::where('value', '=', $marketData['quoteCurrencySymbol'])->first();

                if ($baseCurrency == null) {
                    dump("baseCurrency not found:" . $marketData['baseCurrencyName']);
                    continue;
                }

                if ($quoteCurrencySymbol == null) {
                    dump("quoteCurrencySymbol not found:" . $marketData['quoteCurrencySymbol']);
                    continue;
                }

                $quoteCurrency = $quoteCurrencySymbol->currency;

                $market = array();
                $market['exchange_id'] = $exchangeId;
                $market['base_id'] = $baseCurrency->id;
                $market['quote_id'] = $quoteCurrency->id;
                $market['trade_link'] = $marketData['tradeLink'];
                $market['volume_usd_24h'] = CoinmarketcapDataFilter::filter($marketData['volume_usd_24h']);
                $market['price_usd'] = $marketData['price_usd'];
                $market['has_fees'] = $marketData['has_fees'];
                $market['created_at'] = Carbon::now();
                $marketsToSave[] = $market;
            }
        }
        foreach (array_chunk($marketsToSave, 1000) as $marketsToSaveChunk) {
            Market::insert($marketsToSaveChunk);
        }
    }

    public function updateMarkets($markets) {
        foreach ($markets as $market) {
            unset($market->exchange);
            unset($market->baseCurrency);
            unset($market->quoteCurrency);
        }

        Market::insertOrUpdate($markets->toArray());
    }

    public function updateBatch($markets) {
        Market::insertOrUpdate($markets->toArray());
    }

    public function delete($market)
    {
        $market->delete();
    }
}