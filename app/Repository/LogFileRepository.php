<?php


namespace App\Repository;


use App\Utility\FileTool;

class LogFileRepository
{
    const REGULARLY_LOG_FILE = "regularly-update.log";
    const DAILY_LOG_FILE = "daily-update.log";
    const TAIL_SIZE = 20;

    public function readRegularlyLog() {
        $path = "../" . self::REGULARLY_LOG_FILE;
        return $this->readLog($path);
    }

    public function readDailyLog() {
        $path = "../" . self::DAILY_LOG_FILE;
        return $this->readLog($path);
    }

    private function readLog($path) {
//        return file($path, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
        return FileTool::read_file($path, self::TAIL_SIZE);
    }

    public function clearLog($path) {
        return file_put_contents($path, "");
    }

    public function clearRegularlyLog() {
        $path = "../" . self::REGULARLY_LOG_FILE;
        return $this->clearLog($path);
    }

    public function clearDailyLog() {
        $path = "../" . self::DAILY_LOG_FILE;
        return $this->clearLog($path);
    }
}