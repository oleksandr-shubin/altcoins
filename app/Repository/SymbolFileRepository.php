<?php


namespace App\Repository;


class SymbolFileRepository
{
    public const ALIASES_FILE = 'aliases';

    public function saveAliases($currencies) {

        if ($currencies->isEmpty()) {
            return;
        }

        $file = file(self::ALIASES_FILE);
        foreach ($file as $string) {
            $clearString = trim($string);
            $currencyData = explode("|", $clearString);
            $currencyName = $currencyData[0];
            $currencyAlias = $currencyData[1];

            foreach ($currencies as $currency) {
                if ($currency->name == $currencyName) {
                    $currency->symbols()->create([
                        'currency_id' => $currency->id,
                        'value' => $currencyAlias,
                    ]);
                }
            }
        }
    }
}