<?php


namespace App\Repository;


use App\Exchange;

class ExchangeEloquentRepository
{
    public function saveBatch($exchanges) {
        Exchange::insert($exchanges);
    }

    public function findAll() {
        return Exchange::all();
    }

    public function findAllNoApi() {
        return Exchange::where('has_api', '=', false)->get();
    }

    public function findAllWithApi() {
        return Exchange::where('has_api', '=', true)->get();
    }

    public function findAllWithMarketsWhereHasFees() {
        return Exchange::with('marketsWhereHasFees')->get();
    }

    public function findAllWithMarkets() {
        return Exchange::with('markets')->get();
    }

    public function updateBatch($exchanges) {
        $exchangesToUpdate = $exchanges->map(function($exchange, $key) {
            $exchange->detachAllRelations();
            return $exchange;
        });
        Exchange::insertOrUpdate($exchangesToUpdate->toArray());
    }

    public function findPaginatedExchangesLikeNameSortByVolume($perPage, $name) {
        return Exchange::where('name', 'LIKE', "%$name%")
            ->orderBy("volume_usd_24h", "desc")
            ->simplePaginate($perPage);
    }

    public function delete($exchange) {
        $exchange->delete();
    }
}