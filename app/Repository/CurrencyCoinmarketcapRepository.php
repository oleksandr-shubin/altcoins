<?php


namespace App\Repository;

use App\Currency;
use App\Link;
use GuzzleHttp\Client as GuzzleClient;
use Goutte\Client;
use GuzzleHttp\Promise\EachPromise;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\DomCrawler\Crawler;

class CurrencyCoinmarketcapRepository
{
    public function findAllCryptoCurrencies(){
        $tokenIds = $this->findTokenIds();

        $uri = "https://api.coinmarketcap.com/v1/ticker/?limit=0";
        $client = new GuzzleClient();
        $response = $client->request('GET', $uri);
        $currenciesData = json_decode($response->getBody(), true);

        $currencies = array();
        foreach ($currenciesData as $currencyData) {
            $currency = array();
            $currency['name'] = $currencyData['name'];
            $currency['symbol'] = $currencyData['symbol'];
            $currency['is_fiat'] = false;
            $currency['coinmarketcap_id'] = $currencyData['id'];

            $currency['is_token'] = in_array($currency['coinmarketcap_id'], $tokenIds);
            $currency['has_available_supply_on_coinmarketcap'] = true;

            $currency['price_usd'] = $currencyData['price_usd'];
            $currency['price_btc'] = $currencyData['price_btc'];
            $currency['volume_usd_24h'] = $currencyData['24h_volume_usd'];
            $currency['market_cap_usd'] = $currencyData['market_cap_usd'];
            $currency['available_supply'] = $currencyData['available_supply'];
            $currency['total_supply'] = $currencyData['total_supply'];
            $currency['max_supply'] = $currencyData['max_supply'];
            $currency['percent_change_1h'] = $currencyData['percent_change_1h'];
            $currency['percent_change_24h'] = $currencyData['percent_change_24h'];
            $currency['percent_change_7d'] = $currencyData['percent_change_7d'];
            $currency['last_updated_on_coinmarketcap'] = $currencyData['last_updated'];


            $currencies[] = $currency;
        }
        return $currencies;
    }

    private function findTokenIds() {
        $tokenIds = array();
        $uri = "https://coinmarketcap.com/tokens/views/all/";
        $client = new Client();
        $crawler = $client->request('GET', $uri);

        $tableBodyCrawler = $crawler->filter('table > tbody');
        $rows = $tableBodyCrawler->children();

        foreach($rows as $row) {
            $tokenAnchor = $row->getElementsByTagName('td')[1]->getElementsByTagName('a')[0];
            $tokenLink = $tokenAnchor->getAttribute("href");
            $tokenCoinmarketcapId = explode('/', $tokenLink)[2];
            $tokenIds[] = $tokenCoinmarketcapId;
        }

        return $tokenIds;
    }

//  FIXME
//  Due to error with number of file handlers on linux
//  It's impossible to iterate over all files here
//  You should limit iterations to at least 500 files per once
    public function findAllIcons($currencies) {
        $icons = array();
        $baseUri = "https://coinmarketcap.com/currencies";

        $client = new GuzzleClient();
        $promises = array();

        $start = 0;
        $limit = $currencies->count();

        for ($i = $start; $i < $limit; $i++) {
            $promises[$i] = $client->getAsync($baseUri. "/" . $currencies[$i]['coinmarketcap_id']);
        }

        $eachPromise = new EachPromise($promises, [
            'fulfilled' => function (ResponseInterface $response, $index) use ($currencies, &$icons) {
                $crawler = new Crawler($response->getBody()->getContents());
                $imgCrawler = $crawler->filter('.logo-32x32');
                $iconSrc = $imgCrawler->extract(array('src'))[0];
                $icon = file_get_contents($iconSrc);
                $icons[$currencies[$index]->coinmarketcap_id] = $icon;
            },
            'rejected' => function($reason, $index) use ($currencies) {
                dump("findAllIcons:rejected: " . $currencies[$index]['name']);
            }
        ]);

        $eachPromise->promise()->wait();

        return $icons;
    }

    public function findAllLinks($currencies) {
        $links = collect();
        $baseUri = "https://coinmarketcap.com/currencies";

        $client = new GuzzleClient();
        $promises = array();

        $start = 0;
        $limit = $currencies->count();

        for ($i = $start; $i < $limit; $i++) {
            $promises[$i] = $client->getAsync($baseUri. "/" . $currencies[$i]['coinmarketcap_id']);
        }

        $eachPromise = new EachPromise($promises, [
            'fulfilled' => function (ResponseInterface $response, $index) use ($currencies, &$links) {
                $crawler = new Crawler($response->getBody()->getContents());
                $listItemsCrawler = $crawler->filter('.list-unstyled > li');
                foreach ($listItemsCrawler as $listItem) {
                    $span = $listItem->getElementsByTagName("span")[0];
                    if ($span == null) {
                        continue;
                    }
                    $linkKey = strToUpper($span->getAttribute("title"));
                    if ($linkKey === "RANK" || $linkKey === "TAGS") {
                        continue;
                    }
                    $a = $listItem->getElementsByTagName("a")[0];
                    $link = new Link();
                    $link->currency_id = $currencies[$index]->id;
                    $link->type = Link::TYPES[$linkKey];
                    $link->value = $a->getAttribute("href");
                    $links->push($link);
                }
            },
            'rejected' => function($reason, $index) use ($currencies) {
                dump("findAllLinks:rejected: " . $currencies[$index]['name']);
            }
        ]);

        $eachPromise->promise()->wait();

        return $links;
    }
}