<?php


namespace App\Repository;


use App\Symbol;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Pool;
use GuzzleHttp\Promise\EachPromise;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\DomCrawler\Crawler;

class MarketCoinmarketcapRepository
{

//  Coinmarketcap deleted this coin, but didn't delete all markets with this coin
    const SYMBOL_TO_OMIT = "STR";

    public function findAllByExchanges($exchanges) {
        $markets = array();
        $promises = array();
        $baseUri = "https://coinmarketcap.com/exchanges/";

        $client = new GuzzleClient([
            'headers' => [
                'Connection' => 'close'
            ],
        ]);

        $requests = function($exchanges) use ($baseUri) {
            for ($i = 0; $i < $exchanges->count(); $i++) {
                $uri = $baseUri . $exchanges[$i]['coinmarketcap_id'];
                yield new Request("GET", $uri);
            }
        };

        $pool = new Pool($client, $requests($exchanges), [
            'fulfilled' => function (ResponseInterface $response, $index) use ($exchanges, &$markets) {

                $crawler = new Crawler($response->getBody()->getContents());
                $tableBodyCrawler = $crawler->filter('table > tbody');
                $rows = $tableBodyCrawler->children();

                foreach($rows as $row) {
                    $market = array();

//                    We change baseCurrencySymbol to baseCurrency name because
//                    some coins have same symbols on coinmarketcap.com
                    $baseCurrencyName = $row->getElementsByTagName('td')[1]->getELementsByTagName('a')[0]->textContent;
                    $pairAnchor = $row->getElementsByTagName('td')[2]->getElementsByTagName('a')[0];
                    $pairData = $pairAnchor->textContent;
                    $pairData = explode('/', $pairData);


//                FIXME
//                Omit coin, because of coinmarketcap.com error
                    if ($pairData[0] == self::SYMBOL_TO_OMIT || $pairData[1] == self::SYMBOL_TO_OMIT) {
                        continue;
                    }


                    $market['baseCurrencyName'] = $baseCurrencyName;
                    $market['quoteCurrencySymbol'] = $pairData[1];
                    $market['tradeLink'] = $pairAnchor->getAttribute("href");

                    $market['volume_native_24h'] = $row->getElementsByTagName('td')[3]
                        ->getElementsByTagName('span')[0]->getAttribute('data-native');
                    $market['volume_btc_24h'] = $row->getElementsByTagName('td')[3]
                        ->getElementsByTagName('span')[0]->getAttribute('data-btc');
                    $market['volume_usd_24h'] = $row->getElementsByTagName('td')[3]
                        ->getElementsByTagName('span')[0]->getAttribute('data-usd');

                    $market['price_native'] = $row->getElementsByTagName('td')[4]
                        ->getElementsByTagName('span')[0]->getAttribute('data-native');
                    $market['price_btc'] = $row->getElementsByTagName('td')[4]
                        ->getElementsByTagName('span')[0]->getAttribute('data-btc');
                    $market['price_usd'] = $row->getElementsByTagName('td')[4]
                        ->getElementsByTagName('span')[0]->getAttribute('data-usd');

                    $volumeTextContent = $row->getElementsBytagName('td')[3]
                        ->textContent;

                    $market['has_fees'] = str_contains($volumeTextContent, "**") ? false : true;
                    $markets[$exchanges[$index]->id][] = $market;
                }
            },
            'rejected' => function($reason, $index) use ($exchanges) {
                dump("findAll:rejected: " . $exchanges[$index]->coinmarketcap_id);
            }
        ]);

        $promise = $pool->promise();

        $promise->wait();
        return $markets;
    }
}