<?php


namespace App\Repository;


use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class CurrencyFileRepository
{
    public const FIAT_CURRENCIES_FILE = 'fiat';
    public const ICONS_DIRECTORY = "/img/icons";

    public function findAllFiatCurrencies() {
        $fiatCurrencies = array();
        $file = file(self::FIAT_CURRENCIES_FILE);
        foreach ($file as $string) {
            $clearString = trim($string);
            $fiatCurrency = array();
            $fiatCurrency['name'] = $clearString;
            $fiatCurrency['symbol'] = $clearString;
            $fiatCurrency['is_fiat'] = true;
            $fiatCurrency['is_token'] = false;
            $fiatCurrencies[] = $fiatCurrency;
        }
        return $fiatCurrencies;
    }

    public function saveIcon($icon, $currency) {
        $path = Storage::disk('direct-public')->put(self::ICONS_DIRECTORY, $icon);
        $currency->icon_link = "/" . $path;
        $currency->save();
    }

    public function updateIcon($icon, $currency) {
        $this->deleteIcon($currency);
        $this->saveIcon($icon, $currency);
    }

    public function deleteIcon($currency) {
        $path = substr($currency->icon_link, 1);
        Storage::disk('direct-public')->delete($path);
    }

    public function saveIcons($icons) {
        $paths = array();
        foreach($icons as $currencyCoinmarketcapId => $icon) {
            $path = self::ICONS_DIRECTORY . "/" . $currencyCoinmarketcapId;
            if (Storage::disk('direct-public')->put($path  , $icon)) {
                $paths[$currencyCoinmarketcapId] = $path;
            }
        }
        return $paths;
    }
}