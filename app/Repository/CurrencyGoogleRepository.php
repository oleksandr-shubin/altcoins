<?php


namespace App\Repository;


class CurrencyGoogleRepository
{
    const QUOTE_CURRENCY = "USD";
    const AMOUNT = 1;

    public function findUsdPrice($fiatCurrency) {
        $fromCurrency = urlencode($fiatCurrency->name);
        $toCurrency = urlencode(self::QUOTE_CURRENCY);
        $amount = self::AMOUNT;

        $get = file_get_contents("https://finance.google.com/bctzjpnsun/converter?a=$amount&from=$fromCurrency&to=$toCurrency");
        if (strpos($get, "<span class=bld>") === false) {
            return null;
        }
        $get = explode("<span class=bld>",$get);
        $get = explode("</span>",$get[1]);
        $rate= preg_replace("/[^0-9\.]/", null, $get[0]);
        return $rate;
    }
}