<?php


namespace App\Repository;


use App\Ticker;

class TickerEloquentRepository
{
    public function saveBatch($tickers) {
        $tickersToSave = $tickers->map(function($ticker, $key) {
            $ticker->detachAllRelations();
            return $ticker;
        });
        foreach(array_chunk($tickersToSave->toArray(), 1000) as $tickersChunk) {
            Ticker::insert($tickersChunk);
        }
    }

    public function findAll() {
        return Ticker::all();
    }
}