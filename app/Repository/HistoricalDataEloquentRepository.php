<?php


namespace App\Repository;


use App\HistoricalData;

class HistoricalDataEloquentRepository
{
    public function saveBatch($historicalDatas) {
        foreach(array_chunk($historicalDatas->toArray(),1000) as $historicalDatasChunk) {
            HistoricalData::insert($historicalDatasChunk);
        }
    }
}