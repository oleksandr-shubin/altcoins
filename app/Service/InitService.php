<?php


namespace App\Service;


use App\Repository\CurrencyCoinmarketcapRepository;
use App\Repository\CurrencyEloquentRepository;
use App\Repository\CurrencyFileRepository;
use App\Repository\ExchangeCoinmarketcapRepository;
use App\Repository\ExchangeEloquentRepository;
use App\Repository\HistoricalDataCoinmarketcapRepository;
use App\Repository\HistoricalDataEloquentRepository;
use App\Repository\MarketCoinmarketcapRepository;
use App\Repository\marketEloquentRepository;
use App\Repository\SymbolFileRepository;
use App\User;
use Illuminate\Support\Facades\Hash;

class InitService
{
    private $currencyCoinmarketcapRepository;
    private $currencyFileRepository;
    private $currencyEloquentRepository;
    private $symbolFileRepository;
    private $exchangeCoinmarketcapRepository;
    private $exchangeEloquentRepository;
    private $marketCoinmarketcapRepository;
    private $marketEloquentRepository;
    private $historicalDataCoinmarketcapRepository;
    private $historicalDataEloquentRepository;

    public function __construct(
        CurrencyCoinmarketcapRepository $currencyCoinmarketcapRepository,
        CurrencyFileRepository $currencyFileRepository,
        CurrencyEloquentRepository $currencyEloquentRepository,
        SymbolFileRepository $symbolFileRepository,
        ExchangeCoinmarketcapRepository $exchangeCoinmarektcapRepository,
        ExchangeEloquentRepository $exchangeEloquentRepository,
        MarketCoinmarketcapRepository $marketCoinmarketcapRepository,
        MarketEloquentRepository $marketEloquentRepository,
        HistoricalDataCoinmarketcapRepository $historicalDataCoinmarketcapRepository,
        HistoricalDataEloquentRepository $historicalDataEloquentRepository
    ) {
        $this->currencyCoinmarketcapRepository = $currencyCoinmarketcapRepository;
        $this->currencyFileRepository = $currencyFileRepository;
        $this->currencyEloquentRepository = $currencyEloquentRepository;
        $this->symbolFileRepository = $symbolFileRepository;
        $this->exchangeCoinmarketcapRepository = $exchangeCoinmarektcapRepository;
        $this->exchangeEloquentRepository = $exchangeEloquentRepository;
        $this->marketCoinmarketcapRepository = $marketCoinmarketcapRepository;
        $this->marketEloquentRepository = $marketEloquentRepository;
        $this->historicalDataCoinmarketcapRepository = $historicalDataCoinmarketcapRepository;
        $this->historicalDataEloquentRepository = $historicalDataEloquentRepository;
    }

    public function init() {
        $this->fillCurrenciesTable();

        $this->saveCurrencyLinks();
        $this->saveCurrencyAliases();

        $this->fillExchangesTable();
        $this->fillMarketsTable();
        $this->fillHistoricalDatasTable();

        $this->createDefaultAdmin();
    }

    public function saveCurrencyIcons() {
        $cryptoCurrencies = $this->currencyEloquentRepository->findAllCoinmarketcapCryptoCurrencies();
        $icons = $this->currencyCoinmarketcapRepository->findAllIcons($cryptoCurrencies);
        $paths = $this->currencyFileRepository->saveIcons($icons);
        $this->currencyEloquentRepository->updateCoinmarketcapCryptoCurrenciesSetIconLinks($cryptoCurrencies, $paths);
        dump("currency icons saved from coinmarketcap");
    }


    public function fillCurrenciesTable() {
        $cryptoCurrenciesData = $this->currencyCoinmarketcapRepository->findAllCryptoCurrencies();
        $fiatCurrenciesData = $this->currencyFileRepository->findAllFiatCurrencies();
        $currenciesDataToSave = array_merge($cryptoCurrenciesData, $fiatCurrenciesData);

        $this->currencyEloquentRepository->saveCurrenciesAndMainSymbols($currenciesDataToSave);
        dump("currencies table filled");
    }

    public function saveCurrencyLinks() {
        $cryptoCurrencies = $this->currencyEloquentRepository->findAllCoinmarketcapCryptoCurrencies();
        $links = $this->currencyCoinmarketcapRepository->findAllLinks($cryptoCurrencies);
        $this->currencyEloquentRepository->saveLinks($links);
        dump("currencies links saved");
    }

    public function saveCurrencyAliases() {
        $currencies = $this->currencyEloquentRepository->findAll();
        $this->symbolFileRepository->saveAliases($currencies);
    }

    public function fillExchangesTable() {
        $exchangesData = $this->exchangeCoinmarketcapRepository->findAll();
        $this->exchangeEloquentRepository->saveBatch($exchangesData);
        dump("exchanges table filled");
    }

    public function fillMarketsTable() {
        $exchangesWithoutApi = $this->exchangeEloquentRepository->findAllNoApi();
        $markets = $this->marketCoinmarketcapRepository->findAllByExchanges($exchangesWithoutApi);
        $this->marketEloquentRepository->saveMarkets($markets);
        dump("markets table filled");
    }

    public function fillHistoricalDatasTable() {
        $cryptoCurrencies = $this->currencyEloquentRepository->findAllCoinmarketcapCryptoCurrencies();
        $historicalDatas = $this->historicalDataCoinmarketcapRepository->findAll($cryptoCurrencies);
        $this->historicalDataEloquentRepository->saveBatch($historicalDatas);
        dump("historical datas table filled");
    }

    public function createDefaultAdmin() {
        $user = new User();
        $user->name = "admin";
        $user->password = Hash::make("admin");
        $user->save();
        dump("default admin created");
    }
}