<?php


namespace App\Service;


class CurrencyService
{
    public function findOpenTicker($tickersLastDay)
    {
        return $tickersLastDay->sortBy('created_at')->first();
    }

    public function findCloseTicker($tickersLastDay)
    {
        return $tickersLastDay->sortBy('created_at')->last();
    }

    public function findHighTicker($tickersLastDay)
    {
        return $tickersLastDay->sortBy('price_usd')->last();
    }

    public function findLowTicker($tickersLastDay)
    {
        return $tickersLastDay->sortBy('price_usd')->first();
    }
}