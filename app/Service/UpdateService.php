<?php


namespace App\Service;


use App\HistoricalData;
use App\Repository\CurrencyCoinmarketcapRepository;
use App\Repository\CurrencyEloquentRepository;
use App\Repository\CurrencyFileRepository;
use App\Repository\CurrencyGoogleRepository;
use App\Repository\ExchangeCoinmarketcapRepository;
use App\Repository\ExchangeEloquentRepository;
use App\Repository\HistoricalDataCoinmarketcapRepository;
use App\Repository\HistoricalDataEloquentRepository;
use App\Repository\MarketCoinmarketcapRepository;
use App\Repository\marketEloquentRepository;
use App\Repository\SymbolFileRepository;
use App\Repository\TickerEloquentRepository;
use App\Ticker;
use App\User;
use App\Utility\ArrayTool;
use App\Utility\CoinmarketcapDataFilter;
use App\Utility\CollectionTool;
use App\Utility\DataFilter;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use GuzzleHttp\Client as GuzzleClient;
use Psr\Http\Message\ResponseInterface;

class UpdateService
{
    private $currencyService;
    private $tickerEloquentRepository;
    private $currencyGoogleRepository;
    private $currencyCoinmarketcapRepository;
    private $currencyFileRepository;
    private $currencyEloquentRepository;
    private $symbolFileRepository;
    private $exchangeCoinmarketcapRepository;
    private $exchangeEloquentRepository;
    private $marketCoinmarketcapRepository;
    private $marketEloquentRepository;
    private $historicalDataCoinmarketcapRepository;
    private $historicalDataEloquentRepository;

    public function __construct(
        CurrencyService $currencyService,
        TickerEloquentRepository $tickerEloquentRepository,
        CurrencyGoogleRepository $currencyGoogleRepository,
        CurrencyCoinmarketcapRepository $currencyCoinmarketcapRepository,
        CurrencyFileRepository $currencyFileRepository,
        CurrencyEloquentRepository $currencyEloquentRepository,
        SymbolFileRepository $symbolFileRepository,
        ExchangeCoinmarketcapRepository $exchangeCoinmarektcapRepository,
        ExchangeEloquentRepository $exchangeEloquentRepository,
        MarketCoinmarketcapRepository $marketCoinmarketcapRepository,
        MarketEloquentRepository $marketEloquentRepository,
        HistoricalDataCoinmarketcapRepository $historicalDataCoinmarketcapRepository,
        HistoricalDataEloquentRepository $historicalDataEloquentRepository
    ) {
        $this->currencyService = $currencyService;
        $this->tickerEloquentRepository = $tickerEloquentRepository;
        $this->currencyGoogleRepository = $currencyGoogleRepository;
        $this->currencyCoinmarketcapRepository = $currencyCoinmarketcapRepository;
        $this->currencyFileRepository = $currencyFileRepository;
        $this->currencyEloquentRepository = $currencyEloquentRepository;
        $this->symbolFileRepository = $symbolFileRepository;
        $this->exchangeCoinmarketcapRepository = $exchangeCoinmarektcapRepository;
        $this->exchangeEloquentRepository = $exchangeEloquentRepository;
        $this->marketCoinmarketcapRepository = $marketCoinmarketcapRepository;
        $this->marketEloquentRepository = $marketEloquentRepository;
        $this->historicalDataCoinmarketcapRepository = $historicalDataCoinmarketcapRepository;
        $this->historicalDataEloquentRepository = $historicalDataEloquentRepository;
    }

    public function regularlyUpdate() {
        $this->updateMarketsNativeData();

        $bitcoinPriceUsd = $this->findBitcoinPriceUsd();
        $this->updateFiatCurrenciesWithUsdAndBtcPrices($bitcoinPriceUsd);
        $cryptoCurrenciesToUpdate = $this->updateCurrencies($bitcoinPriceUsd);
        $this->currencyEloquentRepository->updateBatch($cryptoCurrenciesToUpdate);

        //        Tether required more strict update
        $this->updateTether();

        $this->updateMarkets();

        $cryptoCurrenciesToUpdateVolume = $this->updateCurrenciesVolume($bitcoinPriceUsd);
        $this->currencyEloquentRepository->updateBatch($cryptoCurrenciesToUpdateVolume);

        $this->updateCurrenciesPercents();

        $this->updateTickers();
        $this->updateExchanges();
        dump(Carbon::now() . "UTC regularly update finished");
    }

    public function dailyUpdate() {
        $this->regularlyUpdate();
        $this->updateHistoricalData();
        dump(Carbon::now() ."UTC daily update finished");
    }

    private function updateHistoricalData() {
        $historicalDatas = collect();

        $currencies = $this->currencyEloquentRepository->findAllCryptoCurrenciesWithLastDayTickers();
        $currencies->each(function($currency, $key) use ($historicalDatas) {
            $openTicker = $this->currencyService->findOpenTicker($currency->tickersLastDay);
            $highTicker = $this->currencyService->findHighTicker($currency->tickersLastDay);
            $lowTicker = $this->currencyService->findLowTicker($currency->tickersLastDay);

            $historicalData = new HistoricalData();
            $historicalData->currency_id = $currency->id;
            $historicalData->date = Carbon::now()->toDateTimeString();
            $historicalData->open_usd = $openTicker->price_usd;
            $historicalData->high_usd = $highTicker->price_usd;
            $historicalData->low_usd = $lowTicker->price_usd;
            $historicalData->close_usd = $currency->price_usd;
            $historicalData->volume_usd = $currency->volume_usd_24h;
            $historicalData->market_cap_usd = $currency->market_cap_usd;

            $historicalDatas->push($historicalData);
        });

        $this->historicalDataEloquentRepository->saveBatch($historicalDatas);
    }

    private function updateFiatCurrenciesWithUsdAndBtcPrices($bitcoinPriceUsd) {
        $fiatCurrencies = $this->currencyEloquentRepository->findAllFiatCurrencies();
        $updatedFiatCurrencies = $fiatCurrencies->map(function($fiatCurrency, $key) use ($bitcoinPriceUsd) {
            $fiatCurrency->price_usd = $this->currencyGoogleRepository->findUsdPrice($fiatCurrency);
            if ($bitcoinPriceUsd == 0) {
                $fiatCurrency->price_btc = null;
            } else {
                $fiatCurrency->price_btc = $fiatCurrency->price_usd / $bitcoinPriceUsd;
            }
            return $fiatCurrency;
        });
        $this->currencyEloquentRepository->updateBatch($updatedFiatCurrencies);
    }

    private function updateCurrencies($bitcoinPriceUsd) {
//        1. Update CryptoCurrencies
        $cryptoCurrenciesFromCoinmarketcap = $this->currencyCoinmarketcapRepository->findAllCryptoCurrencies();

        $cryptoCurrenciesWithMarketsWhereIsBaseAgainstBitcoin = $this->findAllCryptoCurrenciesWithMarketsWhereIsBaseAgainstBitcoin();
        $cryptoCurrencies = $cryptoCurrenciesWithMarketsWhereIsBaseAgainstBitcoin
            ->map(function($cryptoCurrency, $key) use ($bitcoinPriceUsd, $cryptoCurrenciesFromCoinmarketcap){

            if ($cryptoCurrency->name == "Bitcoin") {
                $cryptoCurrency->price_btc = 1;
            } else {
                $cryptoCurrency->price_btc = $this->findVwap($cryptoCurrency->marketsWhereIsBaseAgainstBitcoin);
            }
            $cryptoCurrency->price_usd = $cryptoCurrency->price_btc * $bitcoinPriceUsd;

            if ($cryptoCurrency->has_available_supply_on_coinmarketcap) {
                $cryptoCurrency->total_supply = $this->findTotalSupplyFromCoinmarketcap($cryptoCurrency, $cryptoCurrenciesFromCoinmarketcap);
                $cryptoCurrency->available_supply = $this->findAvailableSupplyFromCoinmarketcap($cryptoCurrency, $cryptoCurrenciesFromCoinmarketcap);
            } else {
                $cryptoCurrency->total_supply = $this->findTotalSupply($cryptoCurrency);
                $cryptoCurrency->available_supply = $this->findAvailableSupply($cryptoCurrency);
            }
            $cryptoCurrency->market_cap_usd = $cryptoCurrency->price_usd * $cryptoCurrency->available_supply;

//          unset to make cryptoCurrencies liable for updateBatch
            unset($cryptoCurrency->marketsWhereIsBase);
            unset($cryptoCurrency->marketsWhereIsBaseAgainstBitcoin);
            return $cryptoCurrency;
        });


//        2. Update Fiat Currencies
        $fiatCurrencies = $this->currencyEloquentRepository->findAllFiatCurrencies();
        return $cryptoCurrencies;
    }

    private function updateCurrenciesVolume($bitcoinPriceUsd) {
        $cryptoCurrenciesWithMarkets = $this->currencyEloquentRepository->findAllCryptoCurrenciesWithMarketsWhereHasFees();
        $updatedCryptoCurrencies = $cryptoCurrenciesWithMarkets->map(function($cryptoCurrency, $key) use ($bitcoinPriceUsd) {
            $cryptoCurrency->volume_usd_24h = $this->findTotalVolumeUsd($cryptoCurrency->markets);
            $cryptoCurrency->price_btc == 0 ? null : $cryptoCurrency->volume_btc_24h = $cryptoCurrency->volume_usd_24h / $bitcoinPriceUsd;
            $cryptoCurrency->price_usd == 0 ? null : $cryptoCurrency->volume_native_24h = $cryptoCurrency->volume_usd_24h / $cryptoCurrency->price_usd;
            unset($cryptoCurrency->markets);
            return $cryptoCurrency;
        });

        // update rank
        $updatedCryptoCurrencies = $updatedCryptoCurrencies->sortByDesc("market_cap_usd")
            ->values()->map(function($cryptoCurrency, $key){
                
            $cryptoCurrency->rank = $key + 1;
            return $cryptoCurrency;
        });
        return $updatedCryptoCurrencies;
    }

    public function updateTether() {
        $tether = $this->currencyEloquentRepository->findByName("Tether");
        if ($tether == null) {
            return;
        }
        $marketsWhereIsQuote = $tether->marketsWhereIsQuoteAgainstBitcoin()->get();
        $bitcoinPriceUsdt = $this->findVwap($marketsWhereIsQuote);
        $bitcoinPriceUsd = $this->currencyEloquentRepository->findByName("Bitcoin")->price_usd;
        $tetherPriceUsd = $bitcoinPriceUsd/$bitcoinPriceUsdt;

        $tether->price_usd = $tetherPriceUsd;
//        $tether->volume_usd_24h = $tether->volume_native_24h * $tether->price_usd;
        $tether->market_cap_usd = $tether->price_usd * $tether->available_supply;
        $this->currencyEloquentRepository->save($tether);
    }

    public function updateExchanges() {
        $exchanges = $this->exchangeEloquentRepository->findAllWithMarkets();

        // update volume
        $updatedExchanges = $exchanges->map(function($exchange, $key) {
            $volume_usd_24h = $exchange->markets->pluck("volume_usd_24h")->sum();
            $exchange->volume_usd_24h = $volume_usd_24h;
            return $exchange;
        });

        // update rank
        $updatedExchanges = $updatedExchanges->sortByDesc("volume_usd_24h")->map(function($exchange, $key) {
            $exchange->rank = $key + 1;
            return $exchange;
        });
        $this->exchangeEloquentRepository->updateBatch($updatedExchanges);
    }

    private function findAvailableSupplyFromCoinmarketCap($cryptoCurrency, $cryptoCurrenciesFromCoinmarketcap) {
        foreach($cryptoCurrenciesFromCoinmarketcap as $cryptoCurrencyFromCoinmarketcap) {
            if ($cryptoCurrency->coinmarketcap_id == $cryptoCurrencyFromCoinmarketcap['coinmarketcap_id']) {
                return $cryptoCurrencyFromCoinmarketcap['available_supply'];
            }
        }
        return null;
    }

    private function findTotalSupplyFromCoinmarketCap($cryptoCurrency, $cryptoCurrenciesFromCoinmarketcap) {
        foreach($cryptoCurrenciesFromCoinmarketcap as $cryptoCurrencyFromCoinmarketcap) {
            if ($cryptoCurrency->coinmarketcap_id == $cryptoCurrencyFromCoinmarketcap['coinmarketcap_id']) {
                return $cryptoCurrencyFromCoinmarketcap['total_supply'];
            }
        }
        return null;
    }

    public function findAvailableSupply($cryptoCurrency) {
        return $cryptoCurrency->total_supply - $cryptoCurrency->private_supply;
    }

    public function findTotalSupply($cryptoCurrency) {
        $get = @file_get_contents($cryptoCurrency->available_supply_link);
        if ($get === false) {
            return null;
        }
        $decodedResult = json_decode($get, true);
        $availableSupplyResult = ArrayTool::array_column_recursive($decodedResult, $cryptoCurrency->available_supply_index);

        if (empty($availableSupplyResult)) {
            return null;
        }
        $totalSupplyWithBurned = DataFilter::filterFloat($availableSupplyResult[0]);
        $totalSupply = $totalSupplyWithBurned - $cryptoCurrency->burned_supply;
        return $totalSupply;
    }



    //    --------------------------------------------------------------------------------------------------------------------------------------
    private function updateMarketsNativeData() {

//      1. Update form market cap where no api
        $exchangesWithoutApi = $this->exchangeEloquentRepository->findAllNoApi();
        $marketsGroupedByExchangeData = $this->marketCoinmarketcapRepository->findAllByExchanges($exchangesWithoutApi);
        $marketsToUpdateViaCoinmarketcap = $this->marketEloquentRepository->findAllByExchanges($exchangesWithoutApi);
        $marketsUpdatedViaCoinmarketcap = $this->updateMarketsByCoinmarketcapData($marketsToUpdateViaCoinmarketcap, $marketsGroupedByExchangeData);

        $exchangesWithoutApi = $this->exchangeEloquentRepository->findAllWithApi();
        $marketsToUpdateViaApi = $this->marketEloquentRepository->findAllByExchanges($exchangesWithoutApi);
        $marketsUpdatedViaApi = $this->updateMarketsByApi($marketsToUpdateViaApi);

//      3. merge
        $updatedMarkets = CollectionTool::merge($marketsUpdatedViaCoinmarketcap, $marketsUpdatedViaApi);

//      4. Save to database
        $this->marketEloquentRepository->updateBatch($updatedMarkets);
    }

    private function updateMarketsByCoinmarketcapData($markets, $marketsGroupedByExchangeData) {
        foreach ($markets as $market) {
            if (!array_key_exists($market->exchange->id, $marketsGroupedByExchangeData)) {
                unset($market->exchange);
                continue;
            }
            $marketsData = $marketsGroupedByExchangeData[$market->exchange->id];
            foreach ($marketsData as $marketData) {
                if ($market->baseCurrency->name == $marketData['baseCurrencyName']
                    && $market->quoteCurrency->mainSymbol->value == $marketData['quoteCurrencySymbol']) {

                    $market->volume_native_24h = CoinmarketcapDataFilter::filter($marketData['volume_native_24h']);
                    $market->price_native = $marketData['price_native'];
                    $market->updated_at = Carbon::now();
                    $market->has_fees = $marketData['has_fees'] ? 1 : 0;
                }
            }
            $market->detachAllRelations();
        }
        return $markets;
    }

    private function updateMarketsByApi($markets) {
        foreach($markets as $market) {
            if (empty($market->api_link)) {
                continue;
            }

            $client = new GuzzleClient([
                'headers' => [
                    'Connection' => 'close'
                ],
            ]);

            $requests = function ($markets) {
                for ($i = 0; $i < $markets->count(); $i++) {
                    $uri = $markets[$i]->api_link;
                    yield new Request("GET", $uri);
                }
            };

            $pool = new Pool($client, $requests($markets), [
                'fulfilled' => function (ResponseInterface $response, $index) use (&$markets) {
                    $decodedResult = json_decode($response->getBody(), true);
                    $market = $markets[$index];

                    $volume_24hResults = ArrayTool::array_column_recursive($decodedResult, $market->volume_24h_index);
                    $priceResults = ArrayTool::array_column_recursive($decodedResult, $market->price_index);

                    if (empty($volume_24hResults) || empty($priceResults)) {
                        dump("empty: " . $market->api_link);
                    } else {
                        $market->volume_native_24h = DataFilter::filterFloat($volume_24hResults[0]);
                        $market->price_native = DataFilter::filterFloat($priceResults[0]);
                    }
                },
                'rejected' => function ($reason, $index) use ($markets) {
                    dump("updateMarketsByApi:rejected: " . $markets[$index]->coinmarketcap_id);
                }
            ]);

            $promise = $pool->promise();

            $promise->wait();
            return $markets;
        }
    }

    //    --------------------------------------------------------------------------------------------------------------------------------------

    public function findAllCryptoCurrenciesWithMarketsWhereIsBaseAgainstBitcoin() {
        $bitcoinCurrency = $this->currencyEloquentRepository->findByName("bitcoin");
        $cryptoCurrenciesWithMarketsWhereIsBase = $this->currencyEloquentRepository->findAllCryptoCurrenciesWithMarketsWhereIsBase();
        $cryptoCurrenciesWithMarketsWhereIsBaseAgainstBitcoin = $cryptoCurrenciesWithMarketsWhereIsBase
            ->map(function($cryptoCurrencyWithMarketsWhereIsBase, $key) use ($bitcoinCurrency) {
                $marketsWhereIsBaseAgainstBitcoin = $cryptoCurrencyWithMarketsWhereIsBase->marketsWhereIsBase
                    ->filter(function($marketWhereIsBase, $key) use($bitcoinCurrency) {
                        return $marketWhereIsBase->quote_id == $bitcoinCurrency->id;
                    });
                $cryptoCurrencyWithMarketsWhereIsBase->marketsWhereIsBaseAgainstBitcoin = $marketsWhereIsBaseAgainstBitcoin;
                return $cryptoCurrencyWithMarketsWhereIsBase;
            });
        return $cryptoCurrenciesWithMarketsWhereIsBaseAgainstBitcoin;
    }

    public function findBitcoinPriceUsd() {
        $bitcoinUsdMarkets = $this->marketEloquentRepository->findBitcoinUsdMarkets();
        $bitcoinPriceUsd = $this->findVwap($bitcoinUsdMarkets);
        return $bitcoinPriceUsd;
    }

    public function findVwap($markets) {
        $totalVolume = $markets->pluck("volume_native_24h")->sum();
        $volumePriceProductSum = $markets->map(function($market, $key) {
            return $market["volume_native_24h"] * $market["price_native"];
        })->sum();
        if ($totalVolume == 0) {
            return null;
        }
        return $volumePriceProductSum / $totalVolume;
    }

    public function findTotalVolumeNative($markets) {
        return $markets->pluck("volume_native_24h")->sum();
    }

    public function findTotalVolumeUsd($markets) {
        return $markets->pluck("volume_usd_24h")->sum();
    }

//   ----------------------------------------------------------------------------------------------------

    public function updateMarkets() {
        $markets = $this->marketEloquentRepository->findAllWithQuoteCurrencies();
        $updatedMarkets = $markets->map(function($market, $key) {
            $market->price_btc = $market->quoteCurrency->price_btc * $market->price_native;
            $market->price_usd = $market->quoteCurrency->price_usd * $market->price_native;
            $market->volume_usd_24h = $market->price_usd * $market->volume_native_24h;
            $market->volume_btc_24h = $market->price_usd * $market->volume_btc_24h;
            $market->detachAllRelations();
            return $market;
        });
        $this->marketEloquentRepository->updateBatch($updatedMarkets);
    }

    public function updateTickers() {
        $tickers = collect();
        $currencies = $this->currencyEloquentRepository->findAllCryptoCurrencies();
        $currencies->map(function($currency, $key) use ($tickers){
            $ticker = new Ticker();
            $ticker->currency_id = $currency->id;
            $ticker->price_usd = $currency->price_usd;
            $ticker->price_btc = $currency->price_btc;
            $ticker->volume_usd_24h = $currency->volume_usd_24h;
            $ticker->volume_btc_24h = $currency->volume_btc_24h;
            $ticker->volume_native_24h = $currency->volume_native_24h;
            $ticker->created_at = Carbon::now();
            $tickers->push($ticker);
        });
        $this->tickerEloquentRepository->saveBatch($tickers);
    }

    public function updateCurrenciesPercents() {
        $cryptoCurrenciesWithThreeTickers = $this->currencyEloquentRepository->findAllCryptoCurrenciesWithThreeTickers();
        $updatedCryptoCurrencies = $cryptoCurrenciesWithThreeTickers->map(function($cryptoCurrency, $key) {
            $cryptoCurrency->percent_change_1h = $this->findPercentChange($cryptoCurrency->tickerLastHour, $cryptoCurrency->price_usd);
            $cryptoCurrency->percent_change_24h = $this->findPercentChange($cryptoCurrency->tickerLastDay, $cryptoCurrency->price_usd);
            $cryptoCurrency->percent_change_7d = $this->findPercentChange($cryptoCurrency->tickerLastWeek, $cryptoCurrency->price_usd);
            return $cryptoCurrency;
        });
        $this->currencyEloquentRepository->updateBatch($updatedCryptoCurrencies);
    }

    private function findPercentChange($ticker, $priceUsd) {
        if ($ticker == null || $ticker->price_usd == 0) {
            return null;
        }

        return ($priceUsd - $ticker->price_usd) * 100 / $ticker->price_usd;
    }
}
