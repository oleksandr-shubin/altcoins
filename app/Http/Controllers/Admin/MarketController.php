<?php


namespace App\Http\Controllers\Admin;


use App\Exchange;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreMarketRequest;
use App\Http\Requests\UpdateMarketRequest;
use App\Market;
use App\Repository\CurrencyEloquentRepository;
use App\Repository\marketEloquentRepository;
use Illuminate\Http\Request;

class MarketController extends Controller
{
    private $marketEloquentRepository;
    private $currencyEloquentRepository;

    public function __construct(
        MarketEloquentRepository $marketEloquentRepository,
        CurrencyEloquentRepository $currencyEloquentRepository
    ) {
        $this->marketEloquentRepository = $marketEloquentRepository;
        $this->currencyEloquentRepository = $currencyEloquentRepository;
    }

    public function index(Exchange $exchange)
    {
        $markets = $this->marketEloquentRepository->findPaginatedMarketsByExchangeSortByVolume(10, $exchange);
        return view('admin.market.index', compact('exchange', 'markets'));
    }

    public function destroy(Exchange $exchange, Market $market)
    {
        $this->marketEloquentRepository->delete($market);
        return redirect()->back();
    }

    public function edit(Exchange $exchange, Market $market)
    {
        return view('admin.market.edit', compact('market', 'exchange'));
    }

    public function update(Exchange $exchange, Market $market, UpdateMarketRequest $request)
    {
        $market->update($request->validated());
        return redirect()->back()->with('market_update_success', 'Market was successfully updated');
    }

    public function create(Exchange $exchange)
    {
        $currencies = $this->currencyEloquentRepository->findAll();
        return view('admin.market.create', compact('currencies', 'exchange'));
    }

    public function store(Exchange $exchange, StoreMarketRequest $request)
    {
        $exchange->markets()->create($request->validated());
        return redirect()->back()->with('market_create_success', 'Market was successfully created');
    }
}