<?php

namespace App\Http\Controllers\Admin;

use App\Exchange;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreExchangeRequest;
use App\Http\Requests\UpdateExchangeRequest;
use App\Repository\ExchangeEloquentRepository;
use Illuminate\Http\Request;

class ExchangeController extends Controller
{
    private $exchangeEloquentRepository;

    public function __construct(
        ExchangeEloquentRepository $exchangeEloquentRepository
    ) {
        $this->exchangeEloquentRepository = $exchangeEloquentRepository;
    }
    
    public function index(Request $request) {
        $name = $request->input('exchangeName');
        $exchanges = $this->exchangeEloquentRepository->findPaginatedExchangesLikeNameSortByVolume(10, $name);
        return view('admin.exchange.index', compact('exchanges'));
    }

    public function edit(Exchange $exchange)
    {
        return view('admin.exchange.edit', compact('exchange'));
    }

    public function update(UpdateExchangeRequest $request, Exchange $exchange)
    {
        $exchange->update($request->validated());
        return redirect()->back()->with('exchange_update_success', "Exchange successfully updated");
    }

    public function destroy(Exchange $exchange)
    {
        $this->exchangeEloquentRepository->delete($exchange);
        return redirect()->back();
    }

    public function create()
    {
        return view('admin.exchange.create');
    }

    public function store(StoreExchangeRequest $request)
    {
        Exchange::create($request->validated());
        return redirect()->back()->with('exchange_create_success', "Exchange successfully created");
    }
}
