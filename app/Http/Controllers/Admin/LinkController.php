<?php

namespace App\Http\Controllers\Admin;

use App\Currency;
use App\Http\Requests\StoreLinkRequest;
use App\Http\Requests\UpdateLinkRequest;
use App\Link;
use App\Repository\CurrencyEloquentRepository;
use App\Repository\LinkEloquentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LinkController extends Controller
{

    private $linkEloquentRepository;

    public function __construct(
        LinkEloquentRepository $linkEloquentRepository
    ) {
        $this->linkEloquentRepository = $linkEloquentRepository;
    }

    public function index(Currency $currency)
    {
        $links = $this->linkEloquentRepository->findAllByCurrency($currency);
        return view('admin.link.index', compact('links', 'currency'));
    }

    public function destroy(Currency $currency, Link $link)
    {
        $this->linkEloquentRepository->delete($link);
        return redirect()->back();
    }

    public function edit(Currency $currency, Link $link)
    {
        return view('admin.link.edit', compact('link', 'currency'));
    }

    public function update(Currency $currency, Link $link, UpdateLinkRequest $request)
    {
        $link->update($request->validated());
        return redirect()->back()->with('link_update_success', 'Link was successfully updated');
    }

    public function create(Currency $currency)
    {
        return view('admin.link.create', compact( 'currency'));
    }

    public function store(Currency $currency, StoreLinkRequest $request)
    {
        $currency->links()->create($request->validated());
        return redirect()->back()->with('link_create_success', 'link was successfully created');
    }

}
