<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUserPasswordRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repository\LogFileRepository;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    private $logFileRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        LogFileRepository $logFileRepository
    )
    {
        $this->middleware('auth');
        $this->logFileRepository = $logFileRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dailyLog = $this->logFileRepository->readDailyLog();
        $regularlyLog = $this->logFileRepository->readRegularlyLog();
        return view('admin.index', compact('dailyLog', 'regularlyLog'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('admin.profile.edit');
    }

    public function editPassword(User $user)
    {
        return view('admin.profile.editPassword');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $user->name = $request->input('name');
        $user->save();
        return redirect()->back()->with('user_updated_success', "User was successfully updated");
    }

    public function updatePassword(UpdateUserPasswordRequest $request, User $user)
    {
        $user->password = Hash::make($request->input('new_password'));
        $user->save();
        return redirect()->back()->with('user_password_updated_success', "User password successfully udpated");
    }

    public function clearRegularlyLog()
    {
        $this->logFileRepository->clearRegularlyLog();
        return redirect()->back();
    }

    public function clearDailyLog()
    {
        $this->logFileRepository->clearDailyLog();
        return redirect()->back();
    }
}
