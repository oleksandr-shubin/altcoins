<?php

namespace App\Http\Controllers\Admin;

use App\Currency;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCurrencyRequest;
use App\Http\Requests\UpdateCurrencyRequest;
use App\Repository\CurrencyEloquentRepository;
use App\Repository\CurrencyFileRepository;
use App\Symbol;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class CurrencyController extends Controller
{
    private $currencyEloquentRepository;
    private $currencyFileRepository;

    public function __construct(
        CurrencyEloquentRepository $currencyEloquentRepository,
        CurrencyFileRepository $currencyFileRepository
    ) {
        $this->currencyEloquentRepository = $currencyEloquentRepository;
        $this->currencyFileRepository = $currencyFileRepository;
    }

    public function index(Request $request) {
        $name = $request->input('currencyName');
        $currencies = $this->currencyEloquentRepository->findPaginatedCryptoCurrenciesLikeNameSortByMarketCap(10, $name);
        return view('admin.currency.index', compact('currencies'));
    }

    public function edit(Currency $currency)
    {
        return view('admin.currency.edit', compact('currency'));
    }

    public function update(UpdateCurrencyRequest $request, Currency $currency)
    {
        DB::transaction(function() use ($request, $currency) {
           $currency->update($request->validated());
           $currency->mainSymbol->update(['value' => $request->input('main_symbol')]);
           if ($request->has('icon')) {
               $this->currencyFileRepository->updateIcon($request->file('icon'), $currency);
           }
        });
        return redirect()->back()->with('currency_update_success', "Currency was successfully updated");
    }

    public function destroy(Currency $currency)
    {
        $this->currencyEloquentRepository->delete($currency);
        $this->currencyFileRepository->deleteIcon($currency);
        return redirect()->back();
    }

    public function create()
    {
        return view('admin.currency.create');
    }

    public function store(StoreCurrencyRequest $request) {
        DB::transaction(function() use($request) {
            $currency = Currency::create($request->validated());
            Symbol::create([
                'currency_id' => $currency->id,
                'value' => $request->validated()['main_symbol'],
            ]);
            $this->currencyFileRepository->saveIcon($request->file('icon'), $currency);
        });
        return redirect()->back()->with('currency_create_success', "Currency was successfully created");
    }
}
