<?php

namespace App\Http\Controllers;

use App\Currency;
use App\Exchange;
use App\Repository\CurrencyEloquentRepository;
use App\Repository\ExchangeEloquentRepository;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    private $currencyEloquentRepository;
    private $exchangeEloquentRepository;

    public function __construct(
        CurrencyEloquentRepository $currencyEloquentRepository,
        ExchangeEloquentRepository $exchangeEloquentRepository
    ) {
        $this->exchangeEloquentRepository = $exchangeEloquentRepository;
        $this->currencyEloquentRepository = $currencyEloquentRepository;
    }

    public function showExchanges() {
        $exchanges = Exchange::select('id', 'name', 'volume_usd_24h')->get();
        return response()->json($exchanges);
    }

    public function showCurrencies() {
        $currencies = Currency::select('id', 'name', 'icon_link', 'price_usd')->get();
        $currencies->load('mainSymbol');
        return response()->json($currencies);
    }
}
