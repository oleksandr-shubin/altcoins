<?php

namespace App\Http\Controllers;

use App\Currency;
use App\Exchange;
use App\HistoricalData;
use App\Market;
use App\Pair;
use App\Repository\CurrencyCoinmarketcapRepository;
use App\Repository\CurrencyEloquentRepository;
use App\Repository\CurrencyFileRepository;
use App\Repository\CurrencyGoogleRepository;
use App\Repository\ExchangeCoinmarketcapRepository;
use App\Repository\ExchangeEloquentRepository;
use App\Repository\HistoricalDataCoinmarketcapRepository;
use App\Repository\HistoricalDataEloquentRepository;
use App\Repository\MarketCoinmarketcapRepository;
use App\Repository\marketEloquentRepository;
use App\Repository\SymbolFileRepository;
use App\Repository\TickerEloquentRepository;
use App\Service\InitService;
use App\Service\UpdateService;
use App\Symbol;
use App\Ticker;
use App\User;
use App\Utility\ArrayTool;
use App\Utility\CoinmarketcapDataFilter;
use Illuminate\Http\Request;
use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UtilityController extends Controller
{
    private $tickerEloquentRepository;
    private $currencyGooogleRepository;
    private $currencyCoinmarketcapRepository;
    private $currencyFileRepository;
    private $currencyEloquentRepository;
    private $symbolFileRepository;
    private $exchangeCoinmarketcapRepository;
    private $exchangeEloquentRepository;
    private $marketCoinmarketcapRepository;
    private $marketEloquentRepository;
    private $historicalDataCoinmarketcapRepository;
    private $historicalDataEloquentRepository;

    public function __construct(
        TickerEloquentRepository $tickerEloquentRepository,
        CurrencyGoogleRepository $currencyGoogleRepository,
        CurrencyCoinmarketcapRepository $currencyCoinmarketcapRepository,
        CurrencyFileRepository $currencyFileRepository,
        CurrencyEloquentRepository $currencyEloquentRepository,
        SymbolFileRepository $symbolFileRepository,
        ExchangeCoinmarketcapRepository $exchangeCoinmarektcapRepository,
        ExchangeEloquentRepository $exchangeEloquentRepository,
        MarketCoinmarketcapRepository $marketCoinmarketcapRepository,
        MarketEloquentRepository $marketEloquentRepository,
        HistoricalDataCoinmarketcapRepository $historicalDataCoinmarketcapRepository,
        HistoricalDataEloquentRepository $historicalDataEloquentRepository
    ) {
        $this->tickerEloquentRepository = $tickerEloquentRepository;
        $this->currencyGoogleRepository = $currencyGoogleRepository;
        $this->currencyCoinmarketcapRepository = $currencyCoinmarketcapRepository;
        $this->currencyFileRepository = $currencyFileRepository;
        $this->currencyEloquentRepository = $currencyEloquentRepository;
        $this->symbolFileRepository = $symbolFileRepository;
        $this->exchangeCoinmarketcapRepository = $exchangeCoinmarektcapRepository;
        $this->exchangeEloquentRepository = $exchangeEloquentRepository;
        $this->marketCoinmarketcapRepository = $marketCoinmarketcapRepository;
        $this->marketEloquentRepository = $marketEloquentRepository;
        $this->historicalDataCoinmarketcapRepository = $historicalDataCoinmarketcapRepository;
        $this->historicalDataEloquentRepository = $historicalDataEloquentRepository;
    }




//  GET /utility/test
    public function test(UpdateService $updateService, InitService $initService)
    {
        $updateService->regularlyUpdate();
    }


}
