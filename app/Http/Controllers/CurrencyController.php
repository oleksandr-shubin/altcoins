<?php

namespace App\Http\Controllers;

use App\Currency;
use App\Repository\CurrencyEloquentRepository;
use App\Repository\marketEloquentRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use function SebastianBergmann\GlobalState\TestFixture\snapshotFunction;

class CurrencyController extends Controller
{

    const PER_PAGE = 100;
    private $currencyRepository;
    private $marketRepository;

    public function __construct(
        CurrencyEloquentRepository $currencyRepository,
        MarketEloquentRepository $marketRepository
    )
    {
        $this->currencyRepository = $currencyRepository;
        $this->marketRepository = $marketRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
            $cryptoCurrencies = $this->currencyRepository->findAllCryptoCurrenciesWithMainSymbolSortByMarketCap();
            return response()->json($cryptoCurrencies);
        }

        return view('currency.index');
    }

    public function indexPaginate()
    {
        $cryptoCurrencies = $this->currencyRepository->findPaginatedCryptoCurrenciesSortByMarketCap(self::PER_PAGE);

        return view('currency.indexPaginate', compact('cryptoCurrencies'));
    }

    public function indexTokens(Request $request)
    {
        if ($request->ajax()) {
            $tokens = $this->currencyRepository->findAllTokensWithMainSymbolSortByMarketCap();
            return response()->json($tokens);
        }
        return view('currency.tokens', compact('tokens'));
    }

    public function indexTokensPaginate()
    {
        $tokens = $this->currencyRepository->findPaginatedTokensSortByMarketCap(self::PER_PAGE);
        return view('currency.tokensPaginate', compact('tokens'));
    }

    public function indexCoins(Request $request)
    {
        if ($request->ajax()) {
            $coins = $this->currencyRepository->findAllCoinsWIthMainSymbolSortByMarketCap();
            return response()->json($coins);
        }
        return view('currency.coins', compact('coins'));
    }

    public function indexCoinsPaginate()
    {
        $coins = $this->currencyRepository->findPaginatedCoinsSortByMarketCap(self::PER_PAGE);
        return view('currency.coinsPaginate', compact('coins'));
    }

    public function indexVolume()
    {

        $cryptoCurrencies = $this->currencyRepository->findAllCryptoCurrenciesWithTopMarkets()
            ->sortByDesc('volume_usd_24h');

        $totalVolumeUsd = $cryptoCurrencies->sum('volume_usd_24h');

        return view('currency.volume', compact('cryptoCurrencies', 'totalVolumeUsd'));
    }

    public function indexVolumeMonthly()
    {
        $cryptoCurrencies = $this->currencyRepository->findAllCryptoCurrenciesWithMonthlyHistoricalData();
        return view('currency.volumeMonthly', compact ('cryptoCurrencies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Currency $currency)
    {
        return view('currency.showCharts', compact('currency'));
    }

    public function showMarkets(Currency $currency)
    {
        $currency->marketsSortedByVolumeUsd24hDesc = $this->marketRepository
            ->findAllMarketsWithCurrenciesByCurrencyIdSortedByVolumeUsd24hDesc($currency->id);

        $currency->marketsSortedByVolumeUsd24hDesc->transform(function($market, $key) use($currency) {
            if ($market->base_id == $currency->id) {
                $market->price_usd_view = $market->price_usd;
            } else if ($market->price_native == 0) {
                $market->price_usd_view = null;
            } else {
                $market->price_usd_view = $market->price_usd / $market->price_native;
            }
            return $market;
        });
        return view('currency.showMarkets', compact('currency'));
    }

    public function showHistoricalData(Currency $currency)
    {
        return view('currency.showHistoricalData', compact('currency'));
    }

    public function getChart(Currency $currency)
    {
        $chartData = $currency->historicalDatasSortedByDateAsc();

        $chartDataDateFiltered = $chartData->map(function($historicalData, $key) {
            $historicalData->date = Carbon::parse($historicalData->date)->getTimestamp();
            return $historicalData;
        });
        return $chartDataDateFiltered;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
