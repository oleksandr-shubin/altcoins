<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateMarketRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'trade_link' => 'nullable|url|string|max:191',
            'api_link' => 'nullable|required_if:has_api,1|url|string|max:191|unique:markets,api_link,' . $this->segment(5),
            'price_index' => 'nullable|required_with:api_link|string|max:191',
            'volume_24h_index' => 'nullable|required_with:api_link|string|max:191',
            'has_fees' => 'boolean'
        ];
    }
}
