<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreMarketRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'base_id' => 'required|exists:currencies,id',
            'quote_id' => 'required|exists:currencies,id|different:base_id|unique_pair:markets,base_id,' . $this->segment(3),
            'trade_link' => 'nullable|url|string|max:191',
            'api_link' => 'url|string|max:191|unique|required_api_link:' . $this->segment(3) . 'nullable    ',
            'price_index' => 'nullable|required_with:api_link|string|max:191',
            'volume_24h_index' => 'nullable|required_with:api_link|string|max:191',
            'has_fees' => 'required|boolean'
        ];
    }
}
