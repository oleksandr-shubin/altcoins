<?php

namespace App\Http\Requests;

use App\Currency;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateCurrencyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|max:191|unique:currencies,name,' . $this->segment(3),
            'is_token' => 'boolean',
            'main_symbol' => 'string|max:191',
            'coinmarketcap_id' => "required_if:has_available_supply_on_coinmarketcap,1|nullable|string|max:191|unique:currencies,coinmarketcap_id," . $this->segment(3),
            'has_available_supply_on_coinmarketcap' => 'boolean',
            'available_supply_link' => 'required_unless:has_available_supply_on_coinmarketcap,1|nullable|string|max:191',
            'available_supply_index' => 'required_unless:has_available_supply_on_coinmarketcap,1|nullable|string|max:191',
            'burned_supply' => 'required_unless:has_available_supply_on_coinmarketcap,1|nullable|numeric|max:999999999999999',
            'private_supply' => 'required_unless:has_available_supply_on_coinmarketcap,1|nullable|numeric|max:999999999999999',
            'max_supply' => 'nullable|numeric|max:999999999999999',
        ];
    }
}
