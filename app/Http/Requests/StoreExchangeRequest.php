<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreExchangeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:191|unique:exchanges',
            'coinmarketcap_id' => 'required_if:has_api,0|string|max:191|unique:exchanges',
            'has_api' => 'boolean',
            'link' => 'required|url|string|max:191|unique:exchanges',
        ];
    }
}
