<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exchange extends Model
{
    use MassInsertOrUpdatable, RelationsDetachable;

    const TOP_MARKETS_NUMBER = 10;

    protected $fillable = array(
        'name',
        'coinmarketcap_id',
        'has_api',
        'link'
    );

    public function markets() {
        return $this->hasMany(Market::class, 'exchange_id');
    }

    public function topMarkets() {
        return $this->markets()->take(self::TOP_MARKETS_NUMBER);
    }

    public function marketsWhereHasFees() {
        return $this->markets()->where("has_fees", "=", true);
    }
}
