<?php

namespace App\Providers;

use App\Currency;
use App\Exchange;
use App\Market;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

//        extending default validator
        Validator::extend('current_password', function($attribute, $value, $parameters, $validator) {
            return Hash::check($value, Auth::user()->password);
        });

        Validator::extend('unique_pair', function($attribute, $value, $parameters, $validator) {
            $table = $parameters[0];
            $firstColumnIndex = $parameters[1];
            $exchangeId = $parameters[2];

            $firstColumnValue = $validator->getData()[$firstColumnIndex];
            $secondColumnValue = $value;

            $entity = DB::table($table)
                ->where('base_id', '=', $firstColumnValue)
                ->where('quote_id', '=', $secondColumnValue)
                ->where('exchange_id', '=', $exchangeId)
                ->first();
            if ($entity === null) {
                return true;
            } else {
                return false;
            }
        });

        // sharing vars to all views
        $cryptoCurrenciesCount = Currency::where('is_fiat', '=', false)->count();
        $marketsCount = Market::count();
        $exchangesCount = Exchange::count();
        $totalMarketCap = DB::table('currencies')->selectRaw("SUM(market_cap_usd) as totalMarketCap")->first()->totalMarketCap;
        $total24hVolume = DB::table('currencies')->selectRaw("SUM(volume_usd_24h) as total24hVolume")->first()->total24hVolume;

        $bitcoin = Currency::where('name', '=', 'bitcoin')->first();
        $btcDominance = 0;
        if ($totalMarketCap == null || $totalMarketCap == 0 || $bitcoin == null) {
            $btcDominance = null;
        } else {
            $btcDominance = $bitcoin->market_cap_usd / $totalMarketCap * 100;
        }

        View::share(compact('cryptoCurrenciesCount', 'marketsCount', 'exchangesCount', 'totalMarketCap', 'total24hVolume', 'btcDominance'));
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
