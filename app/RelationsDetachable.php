<?php

namespace App;


use Illuminate\Support\Facades\DB;

trait RelationsDetachable
{
    public function detachAllRelations()
    {
       $relations = $this->getRelations();
       foreach ($relations as $key => $values) {
           unset($this->$key);
       }
    }
}