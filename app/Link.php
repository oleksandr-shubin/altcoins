<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    const TYPES = array(
        "WEBSITE" => "website",
        "ANNOUNCEMENT" => "announcement",
        "EXPLORER" => "explorer",
        "MESSAGE BOARD" => "message board",
        "CHAT" => "chat",
        "SOURCE CODE" =>"source code"
    );

    protected $fillable = array(
        'type',
        'value'
    );

    public function currency() {
        return $this->belongsTo(Currency::class, "currency_id");
    }
}
