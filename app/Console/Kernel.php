<?php

namespace App\Console;

use App\Repository\LogFileRepository;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('altcoins:regularlyUpdate')
            ->everyFiveMinutes()
            ->unlessBetween("23:54", "23:56")
            ->withoutOverlapping()
            ->appendOutputTo(LogFileRepository::REGULARLY_LOG_FILE);

        $schedule->command('altcoins:dailyUpdate')
            ->dailyAt("23:55")
            ->appendOutputTo(LogFileRepository::DAILY_LOG_FILE);
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
