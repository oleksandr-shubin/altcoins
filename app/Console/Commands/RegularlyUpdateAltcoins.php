<?php

namespace App\Console\Commands;

use App\Service\UpdateService;
use Illuminate\Console\Command;

class RegularlyUpdateAltcoins extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'altcoins:regularlyUpdate';
    private $updateService;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Regulaly update altcoins';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(UpdateService $updateService)
    {
        parent::__construct();
        $this->updateService = $updateService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->updateService->regularlyUpdate();
    }
}
