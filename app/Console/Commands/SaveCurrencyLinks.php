<?php

namespace App\Console\Commands;

use App\Service\InitService;
use Illuminate\Console\Command;

class SaveCurrencyLinks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'altcoins:saveCurrencyLinks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Saves currency links from coin-cap to DB';
    private $initService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(InitService $initService)
    {
        parent::__construct();
        $this->initService = $initService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->initService->saveCurrencyLinks();
    }
}
