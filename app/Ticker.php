<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticker extends Model
{
    use MassInsertOrUpdatable, RelationsDetachable, Groupable;

    public function currency() {
        return $this->belongsTo(Currency::class, "currency_id");
    }
}
