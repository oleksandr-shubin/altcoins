<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pair extends Model
{
    public function baseCurrency() {
        return $this->belongsTo(Currency::class, 'base_id');
    }

    public function quoteCurrency() {
        return $this->belongsTo(Currency::class, 'quote_id');
    }

    public function stringRepresentation() {
        return $this->baseCurrency->mainSymbol->value . "/" . $this->quoteCurrency->mainSymbol->value;
    }
}
