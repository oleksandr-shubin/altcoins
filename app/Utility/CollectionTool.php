<?php


namespace App\Utility;


class CollectionTool
{
    public static function merge($collection1, $collection2) {
        if ($collection1 == null && $collection2 == null) {
            return collect();
        }

        if ($collection1 == null) {
            return $collection2;
        } else if ($collection2 == null) {
            return $collection1;
        } else {
            return $collection1->merge($collection2);
        }
    }
}