<?php


namespace App\Utility;


class DataFilter
{
    public static function filterFloat($value) {
        if (is_numeric($value)) {
            return (float) $value;
        }
        return null;
    }
}