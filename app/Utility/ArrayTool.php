<?php


namespace App\Utility;


class ArrayTool
{
    public static function array_column_recursive(array $haystack, $needle) {
        $found = [];
        array_walk_recursive($haystack, function($value, $key) use (&$found, $needle) {
            if ($key == $needle)
                $found[] = $value;
        });
        return $found;
    }

    public static function array_intersect_by_key_value (array $arrayToCheck, array $arrayToCompare, $key) {
        $arrayToCheck = array_filter($arrayToCheck, function($arrayToCheckElement) use ($arrayToCompare, $key) {
            foreach ($arrayToCompare as $arrayToCompareElement) {
                if ($arrayToCompareElement[$key] == $arrayToCheckElement[$key]) {
                    return false;
                }
            }
            return true;
        });
        return $arrayToCheck;
    }
}