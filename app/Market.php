<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// If more relationship would be added
// should find all insertOrUpdate methods, applied to this model
// and unset them before save

class Market extends Model
{
    use MassInsertOrUpdatable, Groupable, RelationsDetachable;

    protected $fillable = array(
        'base_id',
        'quote_id',
        'trade_link',
        'api_link',
        'price_index',
        'volume_24h_index',
        'has_fees'
    );

    public function exchange() {
        return $this->belongsTo(Exchange::class, 'exchange_id');
    }

    public function baseCurrency() {
        return $this->belongsTo(Currency::class, 'base_id');
    }

    public function quoteCurrency() {
        return $this->belongsTo(Currency::class, 'quote_id');
    }

    public function pair() {
        return $this->baseCurrency->mainSymbol->value . "/" . $this->quoteCurrency->mainSymbol->value;
    }

    public function volumePercent($totalVolume) {
        if ($totalVolume == null || $totalVolume == 0) {
            return null;
        } else {
            return $this->volume_usd_24h / $totalVolume * 100;
        }
    }
}
