<div class="row">
    <div class="col-md-12">
        <div class="navigation_bar">
            <div id="coinFilter" class="position">

                <div>
                    <div>Market Cap:</div>
                    <select v-model="marketCapFilterValue" class="selectpicker">
                        <option value="0">All</option>
                        <option value="1">$1 Billion+</option>
                        <option value="2">$100 Million - $1 Billion</option>
                        <option value="3">$10 Million - $100 Million</option>
                        <option value="4">$1 Million - $10 Million</option>
                        <option value="5">$100k - $1 Million</option>
                        <option value="6">$0 - $100k</option>
                    </select>
                </div>

                <div>
                    <div>Price:</div>
                    <select v-model="priceFilterValue" class="selectpicker">
                        <option value="0">All</option>
                        <option value="1">$100+</option>
                        <option value="2">$1 - $100</option>
                        <option value="3">$0.01 - $1.00</option>
                        <option value="4">$0.0001 - $0.01</option>
                        <option value="5">$0 - $0.0001</option>
                    </select>
                </div>

                <div>
                    <div>Volume (24h):</div>
                    <select v-model="volumeFilterValue" class="selectpicker">
                        <option value="0">All</option>
                        <option value="1">$10 Million+</option>
                        <option value="2">$1 Million+</option>
                        <option value="3">$100k+</option>
                        <option value="4">$10k+</option>
                        <option value="5">$1k+</option>
                    </select>
                </div>

            </div>

            <div class="next_all">
                <a href="{{ str_replace("/all", "", Request::url()) }}" class="next100">Back to top 100</a>
            </div>
        </div>
    </div>
</div>