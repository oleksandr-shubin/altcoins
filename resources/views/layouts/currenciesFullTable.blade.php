<section id="fullTable" class="full_table">
    <div class="container">

        @include('layouts.fullTableMenu')

        <div class="row">
            <div class="col-md-12">

                <div class="table_head">
                    <div class="h_num">#</div>
                    <div class="h_name">Name</div>
                    <div class="h_cap">Market Cap</div>
                    <div class="h_price">Price</div>
                    <div class="h_supply">Circulating Supply</div>
                    <div class="h_volume">Volume</div>
                    <div class="h_change">Change (1h/24h/7d)</div>
                </div>
            </div>
        </div>

        <div v-for="currency in filteredCurrencies" class="row">
            <div class="col-md-12">
                <div class="crypto_line">
                    <a class="block_link" v-bind:href="'/currency/' + currency.id" style=""></a>

                    <div class="num">@{{ currency.rank }}</div>
                    <div class="name_descr">
                        <div class="logo_line">
                            <div class="cr_logo">
                                <img height="32" width="32" v-bind:src="currency.icon_link" alt="">
                            </div>
                            <div class="cr_name">@{{ currency.name }}</div>
                        </div>
                    </div>
                    <div class="market_cup">@{{ formatIntegerPrice(currency.market_cap_usd) }}</div>
                    <div class="price"> @{{ formatPrice(currency.price_usd, 2) }}</div>
                    <div class="supply">@{{ formatIntegerPrice(currency.available_supply) }}</div>
                    <div class="volume">@{{ formatIntegerPrice(currency.volume_usd_24h) }}</div>
                    <div class="full_change">
                        <span v-bind:class="detectClass(currency.percent_change_1h)">@{{ formatPercent(currency.percent_change_1h) }}</span>
                        <span v-bind:class="detectClass(currency.percent_change_24h)">@{{ formatPercent(currency.percent_change_24h) }}</span>
                        <span v-bind:class="detectClass(currency.percent_change_7d)">@{{ formatPercent(currency.percent_change_7d) }}</span>
                    </div>

                </div>
            </div>
        </div>

    </div>
</section>