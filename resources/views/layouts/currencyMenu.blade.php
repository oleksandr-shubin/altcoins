<ul class="nav nav-tabs" role="navigation">
    <li role="presentation" class="{{ is_active("currency-chart") }}">
        <a href="{{ route('currency-chart', ['currency' => $currency]) }}" aria-controls="home">Charts</a>
    </li>
    <li role="presentation" class="{{ is_active("currency-markets") }}">
        <a href="{{ route('currency-markets', ['currency' => $currency]) }}" aria-controls="profile">Markets</a>
    </li>
    <li role="presentation" class="{{ is_active("currency-historical-data") }}">
        <a href="{{ route('currency-historical-data', ['currency' => $currency]) }}" aria-controls="messages">Historical Data</a>
    </li>
</ul>