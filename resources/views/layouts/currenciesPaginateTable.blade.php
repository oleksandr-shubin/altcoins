<section class="full_table">
    <div class="container">

        @include('layouts.paginatedTableMenu', ['currencies' => $currencies])

        <div class="row">
            <div class="col-md-12">

                <div class="table_head">
                    <div class="h_num">#</div>
                    <div class="h_name">Name</div>
                    <div class="h_cap">Market Cap</div>
                    <div class="h_price">Price</div>
                    <div class="h_supply">Circulating Supply</div>
                    <div class="h_volume">Volume</div>
                    <div class="h_change">Change (24h)</div>
                    <div class="h_gpaph">Price Graph (7d)</div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                @foreach($currencies as $currency)

                    <div class="crypto_line">
                        <a class="block_link" href="/currency/{{ $currency->id }}" style=""></a>

                        <div class="num">{{ $currency->rank }}</div>
                        <div class="name_descr">
                            <div class="logo_line">
                                <div class="cr_logo">
                                    <img height="32" width="32" src="{{ $currency->icon_link }}" alt="">
                                </div>
                                <div class="cr_name">{{ $currency->name }}</div>
                            </div>
                        </div>
                        <div class="market_cup">{{ nullable_money_format($currency->market_cap_usd) }}</div>
                        <div class="price">{{ nullable_money_format($currency->price_usd, 2) }}</div>
                        <div class="supply">{{ nullable_money_format($currency->available_supply) . " " . $currency->mainSymbol->value }}</div>
                        <div class="volume">{{ nullable_money_format($currency->volume_usd_24h) }}</div>
                        <div class="change24h {{ $currency->percent_change_24h < 0 ? "red" : ($currency->percent_change_24h > 0 ? "green" : "")  }}">
                            {{ nullable_percent_format($currency->percent_change_24h) }}
                        </div>
                        <div data-sparkline="{{ $currency->priceLast7Days() }}" class="price_graph"></div>
                    </div>

                @endforeach

            </div>
        </div>

    </div>
</section>

@section('scripts')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script>
/**
 * Create a constructor for sparklines that takes some sensible defaults and merges in the individual
 * chart options. This function is also available from the jQuery plugin as $(element).highcharts('SparkLine').
 */
Highcharts.SparkLine = function (a, b, c) {
    var hasRenderToArg = typeof a === 'string' || a.nodeName,
        options = arguments[hasRenderToArg ? 1 : 0],
        defaultOptions = {
            chart: {
                renderTo: (options.chart && options.chart.renderTo) || this,
                backgroundColor: null,
                borderWidth: 0,
                type: 'area',
                margin: [2, 0, 2, 0],
                width: 120,
                height: 40,
                style: {
                    overflow: 'visible'
                },

                // small optimalization, saves 1-2 ms each sparkline
                skipClone: true
            },
            title: {
                text: ''
            },
            credits: {
                enabled: false
            },
            xAxis: {
                labels: {
                    enabled: false
                },
                title: {
                    text: null
                },
                startOnTick: false,
                endOnTick: false,
                tickPositions: []
            },
            yAxis: {
                endOnTick: false,
                startOnTick: false,
                labels: {
                    enabled: false
                },
                title: {
                    text: null
                },
                tickPositions: [0]
            },
            legend: {
                enabled: false
            },
            tooltip: {
                backgroundColor: null,
                borderWidth: 0,
                shadow: false,
                useHTML: true,
                hideDelay: 0,
                shared: true,
                padding: 0,
                positioner: function (w, h, point) {
                    return { x: point.plotX - w / 2, y: point.plotY - h };
                }
            },
            plotOptions: {
                series: {
                    animation: false,
                    lineWidth: 1,
                    shadow: false,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    marker: {
                        radius: 1,
                        states: {
                            hover: {
                                radius: 2
                            }
                        }
                    },
                    fillOpacity: 0.25,
                    // color: 'yellow'
                    // lineColor: 'yellow',
                },
                column: {
                    negativeColor: '#910000',
                    borderColor: 'silver'
                }
            }
        };

    options = Highcharts.merge(defaultOptions, options);

    return hasRenderToArg ?
        new Highcharts.Chart(a, options, c) :
        new Highcharts.Chart(options, b);
};

var start = +new Date(),
    $tds = $('div[data-sparkline]'),
    fullLen = $tds.length,
    n = 0;

// Creating 153 sparkline charts is quite fast in modern browsers, but IE8 and mobile
// can take some seconds, so we split the input into chunks and apply them in timeouts
// in order avoid locking up the browser process and allow interaction.
function doDraw() {
    var time = +new Date(),
        i,
        len = $tds.length,
        $td,
        stringdata,
        arr,
        data,
        chart;

    for (i = 0; i < len; i += 1) {
        $td = $($tds[i]);
        stringdata = $td.data('sparkline');
        arr = stringdata.split('; ');
        data = $.map(arr[0].split(', '), parseFloat);
        chart = {};

        if (arr[1]) {
            chart.type = arr[1];
        }
        $td.highcharts('SparkLine', {
            series: [{
                data: data,
                pointStart: 1
            }],
            tooltip: {
                headerFormat: '',
                pointFormat: '<b>{point.y}</b> USD'
            },
            chart: chart
        });

        n += 1;
    }
}


doDraw();

</script>
@endsection