<section class="one">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="cr_info">
                    <div class="box_descr">
                        <div class="box_cr_img">
                            <img src="{{ $currency->icon_link }}" alt="" width="64px" height="64px">
                        </div>
                        <div class="box_cr_name">
                            <div class="big_title">
                                <div class="cr_title">{{ $currency->name }}</div>
                                <div class="cr_tag">{{ $currency->mainSymbol->value }}</div>
                            </div>
                            <div class="full_link_line">
                                @foreach($currency->links->sortBy("type") as $link)
                                    <a href="{{ $link->value }}">{{ $link->type }}</a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="box_price">
                        <div class="price_line">
                            <div class="usd_price">${{ number_format($currency->price_usd, 2)}}</div>
                            <div class="usd_change {{ $currency->percent_change_24h > 0 ? "green" : ($currency->percent_change_24h < 0 ? "red" : "") }}">
                                {{ $currency->percent_change_24h }}%
                            </div>
                        </div>
                        <div class="price_line">
                            <div class="btc_price">{{ $currency->price_btc }} BTC</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="stat">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="card">
                    <div class="card_volume">${{ number_format($currency->market_cap_usd) }}<div class="fiat_tag">USD</div></div>
                    <div class="card_title">Market Cap</div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="card_volume">${{ number_format($currency->volume_usd_24h) }}<div class="fiat_tag">USD</div></div>
                    <div class="card_title">Volume (24h)</div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="card_volume">{{ number_format($currency->available_supply) . " " . $currency->mainSymbol->value }}</div>
                    <div class="card_title">Circulating Supply</div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="card_volume">{{ number_format($currency->max_supply) . " " . $currency->mainSymbol->value  }}</div>
                    <div class="card_title">Max Supply</div>
                </div>
            </div>
        </div>
    </div>
</section>