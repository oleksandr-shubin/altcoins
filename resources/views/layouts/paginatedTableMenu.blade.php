<div class="row">
    <div class="col-md-12">
        <div class="navigation_bar">
            <div class="position">
                <div>
                    <select onchange="location = this.value;" title="All" class="selectpicker">
                        <option value="/">Top 100</option>
                        <option value="/all">Full list</option>
                    </select>
                </div>
                <div>
                    <select onchange="location = this.value;" title="Coins" class="selectpicker">
                        <option value="/coin">Top 100</option>
                        <option value="/coin/all">Full list</option>
                    </select>
                </div>
                <div>
                    <select onchange="location = this.value;" title="Tokens" class="selectpicker">
                        <option value="/token">Top 100</option>
                        <option value="/token/all">Full list</option>
                    </select>
                </div>
            </div>

            {{ $currencies->links() }}

        </div>
    </div>
</div>