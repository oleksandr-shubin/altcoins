<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <link rel="stylesheet" href="/css/style.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

</head>
<body>

    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="wrap_head">
                        <div class="left_bar">
                            <div class="bar_item">
                                <div class="dsr">Cryptocurrencies:</div>
                                <a href="/all">{{ $cryptoCurrenciesCount }}</a>
                            </div>
                            <div class="bar_item">
                                <div class="dsr">Markets:</div>
                                <a href="/volume/currency">{{ $marketsCount }}</a>
                            </div>
                            <div class="bar_item">
                                <div class="dsr">Exchanges:</div>
                                <a href="/volume/exchange">{{ $exchangesCount}}</a>
                            </div>
                        </div>
                        <div class="right_bar">
                            <div class="bar_item">
                                <div class="dsr">Market Cap:</div>
                                <a>${{ number_format($totalMarketCap) }}</a>
                            </div>
                            <div class="bar_item">
                                <div class="dsr">24h Vol:</div>
                                <a>${{ number_format($total24hVolume) }}</a>
                            </div>
                            <div class="bar_item">
                                <div class="dsr">BTC Dominance:</div>
                                <a>{{ nullable_percent_format($btcDominance) }}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <nav>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="nav_box">
                        <a href="/">Market cap: All</a>
                        <a href="/coin">Market cap: Coins</a>
                        <a href="/token">Market cap: Tokens</a>
                        <a href="/volume/currency">Volume(24h) Currency</a>
                        <a href="/volume/exchange">Volume(24h) Exchange</a>
                        <a href="/volume/currency/monthly">Volume(Monthly) Currency</a>
                    </div>
                </div>
            </div>
        </div>
    </nav>

    <section v-cloak id="search" class="search">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="search_box">
                        <input v-model="needle" type="text" class="big_search" placeholder="Search Crypto Currencies...">
                        <a class="btn_search"><i class="material-icons">search</i></a>
                    </div>
                    <div class="col-md-12">
                        <div v-show="isVisible" class="drop_search">
                            <div class="cr_drop_list">
                                <div class="cr_title_search">
                                    Cryptocurrency
                                </div>
                                <div class="cr_body_list">

                                    <div v-for="currency in matchCurrencies" class="search_inf">
                                        <a v-bind:href="'/currency/' + currency.id" class="search_link"></a>
                                        <div class="box_descr">
                                            <div class="box_cr_img">
                                                <img v-bind:src="currency.icon_link" alt="" width="36px" height="36px">
                                            </div>
                                            <div class="box_cr_name">
                                                <div class="big_title">
                                                    <div class="cr_title">@{{ currency.name }}</div>
                                                    <div class="cr_tag">@{{ currency.main_symbol.value }}</div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="box_price">
                                            <div class="price_line">
                                                <div class="usd_price">@{{ formatPrice(currency.price_usd) }}</div>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="ex_drop_list">
                                <div class="ex_title">
                                    Exchanges
                                </div>
                                <div class="cr_body_list">

                                    <div v-for="exchange in matchExchanges" class="search_inf">
                                        <a v-bind:href="'/exchange/' + exchange.id" class="search_link"></a>
                                        <div class="box_descr">
                                            <div class="box_cr_img">
                                                {{--<img src="img/btc.png" alt="" width="36px" height="36px">--}}
                                            </div>
                                            <div class="box_cr_name">
                                                <div class="big_title">
                                                    <div class="cr_title">@{{ exchange.name }}</div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="box_price">
                                            <div class="price_line">
                                                <div class="usd_price">@{{ formatPrice(exchange.volume_usd_24h) }}</div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @yield('content')

    <script src="/js/app.js"></script>
    <script src="/js/search.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
    @yield('scripts')
</body>
</html>
