@if ($paginator->hasPages())
    <div class="next_all">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
        @else
            <a href="{{ $paginator->previousPageUrl() }}" class="next100">Previous 100</a>
        @endif

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <a href="{{ $paginator->nextPageUrl() }}" class="next100">Next 100</a>
        @else
        @endif

        <a href="{{ Request::url() . "/all" }}" class="view_all">View All</a>
    </div>
@endif
