@extends('admin.layout')

@section('content')
    <div class="al-content-header">
        Create link for {{ $currency->name }}
    </div>
    <div class="al-form-card col-md-8">

        <form method="POST" action="/admin/currency/{{ $currency->id }}/link">
            @csrf

            <div class="form-group row">
                <label for="type" class="col-sm-4 col-form-label text-md-right">Type</label>

                <div class="col-md-6">
                    <input id="type" type="text" class="form-control{{ $errors->has('type') ? ' is-invalid' : '' }}"
                           name="type" value="{{ old('type') }}">

                    @if ($errors->has('type'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('type') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="value" class="col-sm-4 col-form-label text-md-right">Value</label>

                <div class="col-md-6">
                    <input id="value" type="text" class="form-control{{ $errors->has('value') ? ' is-invalid' : '' }}"
                           name="value" value="{{ old('value') }}">

                    @if ($errors->has('value'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('value') }}</strong>
                    </span>
                    @endif
                </div>
            </div>


            @if(session()->has('link_create_success'))
                <div class="alert alert-success">
                    {{ session()->get('link_create_success') }}
                </div>
            @endif

            <div class="form-group row mb-0">
                <div class="col-md-8 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        Create
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection