@extends('admin.layout')

@section('content')
    <div class="al-content-header">
        {{ $currency->name }} links
    </div>
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Type</th>
                <th scope="col">Value</th>
                <th scope="col">Edit</th>
                <th scope="col">Delete</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($links as $link)
                <tr>
                    <td>{{ $link->type }}</td>
                    <td>{{ $link->value }}</td>
                    <td>
                        <a href="/admin/currency/{{ $currency->id }}/link/{{ $link->id }}/edit" class="btn btn-primary">
                            ...
                        </a>
                    </td>
                    <td>
                        <form class="al-prompt-delete" method="POST" action="/admin/currency/{{ $currency->id }}/link/{{ $link->id }}">
                            @method("DELETE")
                            @csrf
                            <button class="btn btn-danger al-prompt-delete">
                                X
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <form method="GET" action="/admin/currency/{{ $currency->id }}/link/create">
            <button class="btn btn-success">
                Add new
            </button>
        </form>
    </div>

@endsection