@extends('admin.layout')

@section('content')
    <div class="al-content-header">
        Currencies
    </div>
    <form method="GET" action="/admin/currency">
        <input type="text" name="currencyName" class="al-input-text" placeholder="Enter currency name">
        <button class="btn btn-success">
            Search
        </button>
    </form>
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Symbol</th>
                <th scope="col">Coinmarketcap id</th>
                <th scope="col">Available supply link</th>
                <th scope="col">Links</th>
                <th scope="col">Edit</th>
                <th scope="col">Delete</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($currencies as $currency)
                    <tr>
                        <td>{{ $currency->rank }}</td>
                        <td>
                            <img height="32" width="32" src="{{ $currency->icon_link }}">
                            {{ $currency->name }}
                        </td>
                        <td>{{ $currency->mainSymbol->value }}</td>
                        <td>{{ isset($currency->coinmarketcap_id) ? $currency->coinmarketcap_id : "-" }}</td>
                        <td>{{ $currency->available_supply_link ? $currency->available_supply_link : "Coinmarketcap" }}</td>
                        <td>
                            <a href="/admin/currency/{{ $currency->id }}/links" class="btn btn-info">
                                ...
                            </a>
                        </td>
                        <td>
                            <a href="/admin/currency/{{ $currency->id }}/edit" class="btn btn-primary">
                                ...
                            </a>
                        </td>
                        <td>
                            <form class="al-prompt-delete" method="POST" action="/admin/currency/{{ $currency->id }}">
                                @method("DELETE")
                                @csrf
                                <button class="btn btn-danger al-prompt-delete">
                                    X
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $currencies->appends(request()->except('page'))->links("vendor.pagination.simple-admin") }}
        <form method="GET" action="/admin/currency/create">
            <button class="btn btn-success">
                Add new
            </button>
        </form>
    </div>
@endsection