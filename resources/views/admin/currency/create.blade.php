@extends('admin.layout')

@section('content')
    <div class="al-content-header">
        Create currency
    </div>
    <div class="al-form-card col-md-8">

        <form method="POST" action="/admin/currency" enctype="multipart/form-data">
            @csrf

            <div class="form-group row">
                <label for="name" class="col-sm-4 col-form-label text-md-right">Name</label>

                <div class="col-md-6">
                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                           name="name" value="{{ old('name') }}" autofocus>

                    @if ($errors->has('name'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="main_symbol" class="col-sm-4 col-form-label text-md-right">Main symbol</label>

                <div class="col-md-6">
                    <input id="main_symbol" type="text" class="form-control{{ $errors->has('main_symbol') ? ' is-invalid' : '' }}"
                           name="main_symbol" value="{{ old('main_symbol') }}">

                    @if ($errors->has('main_symbol'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('main_symbol') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="icon" class="col-sm-4 col-form-label text-md-right">Upload icon</label>

                <div class="col-md-6">
                    <input id="icon" type="file" class="form-control-file{{ $errors->has('icon') ? ' is-invalid' : '' }}"
                           name="icon">

                    @if ($errors->has('icon'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('icon') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="is_token" class="col-sm-4 col-form-label text-md-right">Is token</label>

                <div class="d-flex align-items-center col-md-6">
                    <input type="hidden" value="0" name="is_token">

                    <input id="is_token" type="checkbox" class="al-checkbox form-control{{ $errors->has('is_token') ? ' is-invalid' : '' }}"
                           name="is_token" value="1">

                    @if ($errors->has('is_token'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('is_token') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="coinmarketcap_id" class="col-sm-4 col-form-label text-md-right">Coinmarketcap id</label>

                <div class="col-md-6">
                    <input id="coinmarketcap_id" type="text" class="form-control{{ $errors->has('coinmarketcap_id') ? ' is-invalid' : '' }}"
                           name="coinmarketcap_id" value="{{ old('coinmarketcap_id') }}">

                    @if ($errors->has('coinmarketcap_id'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('coinmarketcap_id') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="has_available_supply_on_coinmareketcap" class="col-sm-4 col-form-label text-md-right">Has available supply on coinmarketcap</label>

                <div class="col-md-6">
                    <input type="hidden" value="0" name="has_available_supply_on_coinmarketcap">

                    <input id="has_available_supply_on_coinmareketcap" type="checkbox" class="al-checkbox form-control{{ $errors->has('has_available_supply_on_coinmarketcap') ? ' is-invalid' : '' }}"
                           name="has_available_supply_on_coinmarketcap" value="1">

                    @if ($errors->has('has_available_supply_on_coinmarketcap'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('has_available_supply_on_coinmarketcap') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="available_supply_link" class="col-sm-4 col-form-label text-md-right">Available supply link</label>

                <div class="col-md-6">
                    <input id="available_supply_link" type="text" class="form-control{{ $errors->has('available_supply_link') ? ' is-invalid' : '' }}"
                           name="available_supply_link" value="{{ old('available_supply_link') }}">

                    @if ($errors->has('available_supply_link'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('available_supply_link') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="available_supply_index" class="col-sm-4 col-form-label text-md-right">Available supply index</label>

                <div class="col-md-6">
                    <input id="available_supply_index" type="text" class="form-control{{ $errors->has('available_supply_index') ? ' is-invalid' : '' }}"
                           name="available_supply_index" value="{{ old('available_supply_index') }}">

                    @if ($errors->has('available_supply_index'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('available_supply_index') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="burned_supply" class="col-sm-4 col-form-label text-md-right">Burned supply</label>

                <div class="col-md-6">
                    <input id="burned_supply" type="number" class="form-control{{ $errors->has('burned_supply') ? ' is-invalid' : '' }}"
                           name="burned_supply" value="{{ old('burned_supply') }}">

                    @if ($errors->has('burned_supply'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('burned_supply') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="private_supply" class="col-sm-4 col-form-label text-md-right">Private supply</label>

                <div class="col-md-6">
                    <input id="private_supply" type="number" class="form-control{{ $errors->has('private_supply') ? ' is-invalid' : '' }}"
                           name="private_supply" value="{{ old('private_supply') }}">

                    @if ($errors->has('private_supply'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('private_supply') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="max_supply" class="col-sm-4 col-form-label text-md-right">Max supply</label>

                <div class="col-md-6">
                    <input id="max_supply" type="number" class="form-control{{ $errors->has('max_supply') ? ' is-invalid' : '' }}"
                           name="max_supply" value="{{ old('max_supply') }}">

                    @if ($errors->has('max_supply'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('max_supply') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            @if(session()->has('currency_create_success'))
                <div class="alert alert-success">
                    {{ session()->get('currency_create_success') }}
                </div>
            @endif

            <div class="form-group row mb-0">
                <div class="col-md-8 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        Create
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection