<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <header class="navbar navbar-expand navbar-light al-navbar">
            <a class="navbar-brand" href="{{ url('/admin') }}">
                {{ config('app.name', 'Laravel') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </header>

            <div class="container-fluid">
                <div class="row flex-xl-nowrap">
                    @guest
                    @else
                    <div class="col-12 col-md-3 col-xl-2 al-sidebar">

                        <ul class="al-sidenav">
                            <li class="al-sidenav-item"><a class="al-sidenav-link" href="/admin">Info</a></li>
                            <li class="al-sidenav-item"><a class="al-sidenav-link" href="/admin/{{ Auth::id() }}/edit">Change login</a></li>
                            <li class="al-sidenav-item"><a class="al-sidenav-link" href="/admin/{{ Auth::id() }}/edit/password">Change password</a></li>
                            <li class="al-sidenav-item"><a class="al-sidenav-link" href="/admin/currency">Currencies</a></li>
                            <li class="al-sidenav-item"><a class="al-sidenav-link" href="/admin/exchange">Exchanges</a></li>
                        </ul>
                    </div>
                    @endguest

                    <div class="col-12 col-md-9 col-xl-10 al-content">
                        @yield('content')
                    </div>

                </div>
            </div>

    </div>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
