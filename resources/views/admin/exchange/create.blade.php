@extends('admin.layout')

@section('content')
    <div class="al-content-header">
        Create exchange
    </div>
    <div class="al-form-card col-md-8">

        <form method="POST" action="/admin/exchange">
            @csrf

            <div class="form-group row">
                <label for="name" class="col-sm-4 col-form-label text-md-right">Name</label>

                <div class="col-md-6">
                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                           name="name" value="{{ old('name') }}" autofocus>

                    @if ($errors->has('name'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="coinmarketcap_id" class="col-sm-4 col-form-label text-md-right">Coinmarketcap id</label>

                <div class="col-md-6">
                    <input id="coinmarketcap_id" type="text" class="form-control{{ $errors->has('coinmarketcap_id') ? ' is-invalid' : '' }}"
                           name="coinmarketcap_id" value="{{ old('coinmarketcap_id') }}">

                    @if ($errors->has('coinmarketcap_id'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('coinmarketcap_id') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="has_api" class="col-sm-4 col-form-label text-md-right">Has api</label>

                <div class="d-flex align-items-center col-md-6">
                    <input type="hidden" value="0" name="has_api">

                    <input id="has_api" type="checkbox" class="al-checkbox form-control{{ $errors->has('has_api') ? ' is-invalid' : '' }}"
                           name="has_api" value="1">

                    @if ($errors->has('has_api'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('has_api') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="link" class="col-sm-4 col-form-label text-md-right">Link</label>

                <div class="col-md-6">
                    <input id="link" type="text" class="form-control{{ $errors->has('link') ? ' is-invalid' : '' }}"
                           name="link" value="{{ old('link') }}">

                    @if ($errors->has('link'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('link') }}</strong>
                    </span>
                    @endif
                </div>
            </div>


            @if(session()->has('exchange_create_success'))
                <div class="alert alert-success">
                    {{ session()->get('exchange_create_success') }}
                </div>
            @endif

            <div class="form-group row mb-0">
                <div class="col-md-8 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        Create
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection