@extends('admin.layout')

@section('content')
    <div class="al-content-header">
        Exchanges
    </div>
    <form method="GET" action="/admin/exchange">
        <input type="text" name="exchangeName" class="al-input-text" placeholder="Enter exchange name">
        <button class="btn btn-success">
            Search
        </button>
    </form>
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Coinmarketcap id</th>
                <th scope="col">Link</th>
                <th scope="col">Has api</th>
                <th scope="col">Markets</th>
                <th scope="col">Edit</th>
                <th scope="col">Delete</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($exchanges as $exchange)
                <tr>
                    <td>{{ $exchange->rank }}</td>
                    <td>{{ $exchange->name }}</td>
                    <td>{{ $exchange->coinmarketcap_id }}</td>
                    <td>
                        <a href="{{ $exchange->link }}">{{ $exchange->link }}</a></td>
                    <td>{{ $exchange->has_api ? "Yes" : "No" }}</td>
                    <td>
                        <a href="/admin/exchange/{{ $exchange->id }}/markets" class="btn btn-info">
                            ...
                        </a>
                    </td>
                    <td>
                        <a href="/admin/exchange/{{ $exchange->id }}/edit" class="btn btn-primary">
                            ...
                        </a>
                    </td>
                    <td>
                        <form class="al-prompt-delete" method="POST" action="/admin/exchange/{{ $exchange->id }}">
                            @method("DELETE")
                            @csrf
                            <button class="btn btn-danger">
                                X
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $exchanges->appends(request()->except('page'))->links("vendor.pagination.simple-admin") }}
        <form method="GET" action="/admin/exchange/create">
            <button class="btn btn-success">
                Add new
            </button>
        </form>
    </div>

@endsection