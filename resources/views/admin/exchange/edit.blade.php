@extends('admin.layout')

@section('content')
    <div class="al-content-header">
        Edit exchange
    </div>
    <div class="al-form-card col-md-8">

        <form method="POST" action="/admin/exchange/{{ $exchange->id }}">
            @csrf
            @method('PATCH')

            <div class="form-group row">
                <label for="name" class="col-sm-4 col-form-label text-md-right">Name</label>

                <div class="col-md-6">
                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                           name="name" value="{{ null != old('name') ? old('name') : $exchange->name }}" autofocus>

                    @if ($errors->has('name'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="coinmarketcap_id" class="col-sm-4 col-form-label text-md-right">Coinmarketcap id</label>

                <div class="col-md-6">
                    <input id="coinmarketcap_id" type="text" class="form-control{{ $errors->has('coinmarketcap_id') ? ' is-invalid' : '' }}"
                           name="coinmarketcap_id" value="{{ null != old('coinmarketcap_id') ? old('coinmarketcap_id') : $exchange->coinmarketcap_id }}">

                    @if ($errors->has('coinmarketcap_id'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('coinmarketcap_id') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="has_api" class="col-sm-4 col-form-label text-md-right">Has api</label>

                <div class="d-flex align-items-center col-md-6">
                    <input type="hidden" value="0" name="has_api">

                    <input id="has_api" type="checkbox" class="al-checkbox form-control{{ $errors->has('has_api') ? ' is-invalid' : '' }}"
                           name="has_api" value="1"
                            {{ $exchange->has_api ? "checked" : "" }}>

                    @if ($errors->has('has_api'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('has_api') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="link" class="col-sm-4 col-form-label text-md-right">Link</label>

                <div class="col-md-6">
                    <input id="link" type="text" class="form-control{{ $errors->has('link') ? ' is-invalid' : '' }}"
                           name="link" value="{{ null != old('link') ? old('link') : $exchange->link }}">

                    @if ($errors->has('link'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('link') }}</strong>
                    </span>
                    @endif
                </div>
            </div>


            @if(session()->has('exchange_update_success'))
                <div class="alert alert-success">
                    {{ session()->get('exchange_update_success') }}
                </div>
            @endif

            <div class="form-group row mb-0">
                <div class="col-md-8 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        Update
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection