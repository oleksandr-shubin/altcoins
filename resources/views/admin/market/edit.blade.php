@extends('admin.layout')

@section('content')
    <div class="al-content-header">
        Edit market
    </div>
    <div class="al-form-card col-md-8">

        <form method="POST" action="/admin/exchange/{{ $market->exchange->id }}/market/{{ $market->id }}">
            @csrf
            @method('PATCH')

            <div class="form-group row">
                <label for="base_id" class="col-sm-4 col-form-label text-md-right">Base currency</label>

                <div class="col-md-6">
                    <input class="form-control" value="{{ $market->baseCurrency->name }}" readonly>

                </div>
            </div>

            <div class="form-group row">
                <label for="quote_id" class="col-sm-4 col-form-label text-md-right">Quote currency</label>

                <div class="col-md-6">
                    <input class="form-control" value="{{ $market->quoteCurrency->name }}" readonly>
                </div>
            </div>

            <div class="form-group row">
                <label for="trade_link" class="col-sm-4 col-form-label text-md-right">Trade link</label>

                <div class="col-md-6">
                    <input id="trade_link" type="text" class="form-control{{ $errors->has('trade_link') ? ' is-invalid' : '' }}"
                           name="trade_link" value="{{ null !== old('trade_link') ? old('trade_link') : $market->trade_link }}">

                    @if ($errors->has('trade_link'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('trade_link') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="has api" class="col-sm-4 col-form-label text-md-right">Exchange has api (1:Yes/0:No)</label>

                <div class="col-md-6">
                    <input id="has_api" type="text" class="form-control" value="{{ $exchange->has_api }}" name="has_api" readonly>
                </div>
            </div>



            <div class="form-group row">
                <label for="api_link" class="col-sm-4 col-form-label text-md-right">Api link</label>

                <div class="col-md-6">
                    <input id="api_link" type="text" class="form-control{{ $errors->has('api_link') ? ' is-invalid' : '' }}"
                           name="api_link" value="{{ null !== old('api_link') ? old('api_link') : $market->api_link }}">

                    @if ($errors->has('api_link'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('api_link') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="price_index" class="col-sm-4 col-form-label text-md-right">Price index</label>

                <div class="col-md-6">
                    <input id="price_index" type="text" class="form-control{{ $errors->has('price_index') ? ' is-invalid' : '' }}"
                           name="price_index" value="{{ null !== old('price_index') ? old('price_index') : $market->price_index }}">

                    @if ($errors->has('price_index'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('price_index') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="volume_24h_index" class="col-sm-4 col-form-label text-md-right">Volume index</label>

                <div class="col-md-6">
                    <input id="volume_24h_index" type="text" class="form-control{{ $errors->has('volume_24h_index') ? ' is-invalid' : '' }}"
                           name="volume_24h_index" value="{{ null !== old('volume_24h_index') ? old('volume_24h_index') : $market->volume_24h_index }}">

                    @if ($errors->has('volume_24h_index'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('volume_24h_index') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="has_fees" class="col-sm-4 col-form-label text-md-right">Has fees</label>

                <div class="d-flex align-items-center col-md-6">
                    <input type="hidden" value="0" name="has_fees">

                    <input id="has_fees" type="checkbox" class="al-checkbox form-control{{ $errors->has('has_fees') ? ' is-invalid' : '' }}"
                           name="has_fees" value="1" {{ $market->has_fees ? "checked" : '' }}>

                    @if ($errors->has('has_fees'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('has_fees') }}</strong>
                    </span>
                    @endif
                </div>
            </div>


            @if(session()->has('market_update_success'))
                <div class="alert alert-success">
                    {{ session()->get('market_update_success') }}
                </div>
            @endif

            <div class="form-group row mb-0">
                <div class="col-md-8 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        Update
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection