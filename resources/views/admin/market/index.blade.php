@extends('admin.layout')

@section('content')
    <div class="al-content-header">
        {{ $exchange->name }} markets
    </div>
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Pair</th>
                <th scope="col">Api link</th>
                <th scope="col">Trade link</th>
                <th scope="col">Has fees</th>
                <th scope="col">Edit</th>
                <th scope="col">Delete</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($markets as $market)
                <tr>
                    <td>{{ $market->pair() }}</td>
                    <td>{{ $market->api_link ? $market->api_link : "-" }}</td>
                    <td>{{ $market->trade_link ? $market->trade_link : "-" }}</td>
                    <td>{{ $market->has_fees ? "Yes" : "No" }}</td>
                    <td>
                        <a href="/admin/exchange/{{ $exchange->id }}/market/{{ $market->id }}/edit" class="btn btn-primary">
                            ...
                        </a>
                    </td>
                    <td>
                        <form class="al-prompt-delete" method="POST" action="/admin/exchange/{{ $exchange->id }}/market/{{ $market->id }}">
                            @method("DELETE")
                            @csrf
                            <button class="btn btn-danger al-prompt-delete">
                                X
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $markets->appends(request()->except('page'))->links("vendor.pagination.simple-admin") }}
        <form method="GET" action="/admin/exchange/{{ $exchange->id }}/market/create">
            <button class="btn btn-success">
                Add new
            </button>
        </form>
    </div>

@endsection