@extends('admin.layout')

@section('content')
    <div class="al-content-header">
        Main admin page
    </div>

    <div class="d-flex flex-row justify-content-between">

        <div class="d-flex flex-column w-50">

            <header class="al-card-header border-right">Regularly update log</header>
            <div class="al-card-body border-right border-bottom">
                @foreach ($regularlyLog as $string)
                    <p>{{ $string }}</p>
                @endforeach
            </div>
            <form action="/admin/log/regularly" method="POST">
                @method('DELETE')
                @csrf
                <button class="my-2 btn btn-danger">
                    Clear log
                </button>
            </form>
        </div>

        <div class="d-flex flex-column w-50">
            <header class="al-card-header">Daily update log</header>

            <div class="al-card-body border-bottom">
                @foreach ($dailyLog as $string)
                    <p>{{ $string }}</p>
                @endforeach
            </div>
            <form action="/admin/log/daily" method="POST">
                @method('DELETE')
                @csrf
                <button class="my-2 btn btn-danger">
                    Clear log
                </button>
            </form>
        </div>
    </div>
@endsection
