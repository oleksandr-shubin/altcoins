@extends('admin.layout')

@section('content')
    <div class="al-content-header">
        Change password
    </div>
    <div class="al-form-card col-md-8">

        <form method="POST" action="/admin/{{ Auth::id() }}/password">
            @csrf
            {{ method_field('PATCH') }}

            <div class="form-group row">
                <label for="new_password" class="col-md-4 col-form-label text-md-right">New password</label>

                <div class="col-md-6">
                    <input id="new_password" type="password" class="form-control{{ $errors->has('new_password') ? ' is-invalid' : '' }}" name="new_password" required>

                    @if ($errors->has('new_password'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('new_password') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="new_password_confirmation" class="col-md-4 col-form-label text-md-right">Repeat new password (Confirmation)</label>

                <div class="col-md-6">
                    <input id="new_password_confirmation" type="password" class="form-control{{ $errors->has('new_password_confirmation') ? ' is-invalid' : '' }}" name="new_password_confirmation" required>

                    @if ($errors->has('new_password_confirmation'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('new_password_confirmation') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <hr />

            <div class="form-group row">
                <label for="password" class="col-md-4 col-form-label text-md-right">Current password (for sercurity reasons)</label>

                <div class="col-md-6">
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                    @if ($errors->has('password'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            @if(session()->has('user_password_updated_success'))
                <div class="alert alert-success">
                    {{ session()->get('user_password_updated_success') }}
                </div>
            @endif

            <div class="form-group row mb-0">
                <div class="col-md-8 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        Update
                    </button>

                </div>
            </div>
        </form>
    </div>
@endsection