@extends('admin.layout')

@section('content')
    <div class="al-content-header">
        Change login
    </div>
    <div class="al-form-card col-md-8">

        <form method="POST" action="/admin/{{ Auth::id() }}">
            @csrf
            {{ method_field('PATCH') }}

            <div class="form-group row">
                <label for="name" class="col-sm-4 col-form-label text-md-right">Name</label>

                <div class="col-md-6">
                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ null !== old('name') ? old('name') : Auth::user()->name }}" required autofocus>

                    @if ($errors->has('name'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <hr />

            <div class="form-group row">
                <label for="password" class="col-md-4 col-form-label text-md-right">Current password (for sercurity reasons)</label>

                <div class="col-md-6">
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                    @if ($errors->has('password'))
                        <span class="invalid-feedback">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            @if(session()->has('user_update_success'))
                <div class="alert alert-success">
                    {{ session()->get('user_update_success') }}
                </div>
            @endif

            <div class="form-group row mb-0">
                <div class="col-md-8 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        Update
                    </button>

                </div>
            </div>
        </form>
    </div>
@endsection