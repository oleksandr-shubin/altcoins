@extends('layouts.layout')
@section('title', $exchange->name)

@section('content')
    <section class="one">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="cr_info">
                        <div class="box_descr">
                            <div class="box_cr_img">
                                {{--<img src="img/294.png" alt="" width="64px" height="64px">--}}
                            </div>
                            <div class="box_cr_name">
                                <div class="big_title">
                                    <div class="cr_title">{{ $exchange->name }}</div>
                                </div>
                                <div class="full_link_line">
                                    {{--@foreach($exchange->links->sortBy("type") as $link)--}}
                                        {{--<a href="{{ $link->value }}">{{ $link->type }}</a>--}}
                                    {{--@endforeach--}}
                                </div>
                            </div>
                        </div>
                        <div class="box_price">
                            <div class="price_line">
                                <div class="usd_price">${{ number_format($exchange->volume_usd_24h, 2)}}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="card1">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Currency</th>
                                <th scope="col">Pair</th>
                                <th scope="col">Volume (24h)</th>
                                <th scope="col">Price</th>
                                <th scope="col">Volume (%)</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 1 ?>
                            @foreach ($markets as $market)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>
                                        <a href="/currency/{{ $market->baseCurrency->id }}">
                                            {{ $market->baseCurrency->name }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ $market->trade_link }}">
                                            {{ $market->pair() }}
                                        </a>
                                    </td>
                                    <td>
                                        @if(!$market->has_fees)
                                            **
                                        @endIf
                                        {{ "$" . number_format($market->volume_usd_24h) }}
                                    </td>
                                    <td>{{ "$" . number_format($market->price_usd, 2) }}</td>
                                    <td>{{ number_format($market->volumePercent($exchange->volume_usd_24h), 2) . "%" }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        ** Volume excluded - no trading fees
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
@endsection