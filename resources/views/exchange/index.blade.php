@extends('layouts.layout')
@section('title', 'Exchanges')

@section('content')
    <section class="ex24">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @foreach($exchanges as $exchange)
                        <div class="card_ex">
                            <div class="card_ex_head">
                                <div class="card_ex_num">{{ $exchange->rank }}.</div>
                                <div class="card_ex_name">{{ $exchange->name }}</div>
                            </div>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Currency</th>
                                    <th scope="col">Pair</th>
                                    <th scope="col">Volume (24h)</th>
                                    <th scope="col">Price</th>
                                    <th scope="col">Volume (%)</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1 ?>
                                @foreach ($exchange->topMarkets as $market)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>
                                            <a href="/currency/{{ $market->baseCurrency->id }}">
                                                {{ $market->baseCurrency->name }}
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{ $market->trade_link }}">
                                                {{ $market->pair() }}
                                            </a>
                                        </td>
                                        <td>{{ "$" . number_format($market->volume_usd_24h , 3) }}</td>
                                        <td>{{ "$" . number_format($market->price_usd, 3) }}</td>
                                        <td>{{ number_format($market->volumePercent($exchange->volume_usd_24h), 2) . "%" }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
@endsection