@extends('layouts.layout')
@section('title', $currency->name)
@section('content')
    @include('layouts.currenciesData', $currency)
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="card1">

                        @include('layouts.currencyMenu', $currency)

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div id="container" style="height:620px" class="mx-auto"></div>

                            <div class="btn-group">
                                <button id="linear" type="button" class="btn btn-secondary">Linear Scale</button>
                                <button id="log" type="button" class="btn btn-secondary">Log Scale</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script src="https://code.highcharts.com/stock/highstock.js"></script>
    <script src="https://code.highcharts.com/stock/modules/drag-panes.js"></script>
    <script src="https://code.highcharts.com/stock/modules/exporting.js"></script>
    <script>
        var getTimezoneOffset = (function() {
            var offset = new Date().getTimezoneOffset();
            return function() {
                return offset;
            };
        })();
        $.getJSON("/currency/{{ $currency->id }}/get-chart", function (historicalDatas) {
            var prices = new Array();
            var volumes = new Array();
            var marketCaps = new Array();

            historicalDatas = Object.values(historicalDatas);

            historicalDatas.forEach(function(historicalData) {
                prices.push([
                    historicalData.date * 1000,
                    parseFloat(historicalData.close_usd)
                ]);

                volumes.push([
                    historicalData.date * 1000,
                    parseFloat(historicalData.volume_usd)
                ]);

                marketCaps.push([
                    historicalData.date * 1000,
                    parseFloat(historicalData.market_cap_usd)
                ]);
            });

            Highcharts.setOptions({
                lang: {
                    thousandsSep: ',',
                    numericSymbols: ["k" , "M" , "B", "T", "P", "E"],
                },
                global: {
                    timezoneOffset: getTimezoneOffset(),
                }
            });

            chart = Highcharts.stockChart('container', {
                chart: {
                    type: 'line',
                },

                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 600
                        },
                        chartOptions: {
                            chart: {
                                zoomType: 'none'
                            },
                            credits: {
                                enabled: false
                            },
                            legend: {
                                itemDistance: 30,
                                itemMarginBottom: 15,
                                itemStyle: {
                                    fontSize: '15px'
                                }
                            },
                            scrollbar: {
                                enabled: false
                            },
                            yAxis: [
                                {
                                    title: {
                                        enabled: false
                                    },
                                    labels: {
                                        align: 'left',
                                        x: 0
                                    }
                                },
                                {
                                    title: {
                                        enabled: false
                                    },
                                    labels: {
                                        align: 'right',
                                        x: 0
                                    }
                                },
                                {
                                    title: {
                                        enabled: false
                                    },
                                    labels: {
                                        align: 'left',
                                        x: 0
                                    }
                                }
                            ]
                        }
                    }]
                },

                tooltip: {
                    shared: true,
                    split: false,
                    xDateFormat: '%A, %b %d %Y',

                },

                legend: {
                    enabled: true,
                    floating: false,
                },

                rangeSelector: {
                    allButtonsEnabled: true,
                    buttons: [{
                        type: 'day',
                        count: 1,
                        text: '1d'
                    }, {
                        type: 'week',
                        count: 1,
                        text: '7d'
                    }, {
                        type: 'month',
                        count: 1,
                        text: '1m'
                    }, {
                        type: 'month',
                        count: 3,
                        text: '3m'
                    }, {
                        type: 'year',
                        count: 1,
                        text: '1y'
                    }, {
                        type: 'ytd',
                        count: 1,
                        text: 'YTD'
                    }, {
                        type: 'all',
                        text: 'ALL'
                    }],
                    selected: 6,
                    inputEnabled: true,
                    enabled: true,
                    inputStyle: {
                        color: '#757575'
                    },
                    labelStyle: {
                        color: '#757575'
                    }
                },

                title: {
                    text: "{{ $currency->name }} Charts",
                    align: 'left'
                },

                xAxis: [
                    {
                        minRange: 24 * 3600 * 1000,
                    }
                ],

                yAxis: [
                    {
                        labels: {
                            formatter: function() {
                                return '$' + this.axis.defaultLabelFormatter.call(this);
                            },
                            align: 'right',
                        },
                        title: {
                            text: 'Market Cap',
                        },
                        showEmpty: false,
                        height: '80%',
                        opposite: false,
                        floor: 0,
                    },

                    {
                        labels: {
                            formatter: function() {
                                return '$' + this.value.toLocaleString(undefined, {
                                    minimumFractionDigits: 2,
                                    maximumFractionDigits: 2
                                });
                            },
                            align: 'left',
                            x: 15
                        },
                        title: {
                            text: 'Price (USD)',
                        },
                        showEmpty: false,
                        height: '80%',
                        lineWidth: 0,
                        opposite: true,
                        floor: 0,
                    },

                    {
                        labels: {
                            align: 'right'
                        },
                        title: {
                            text: '24h Vol',
                        },
                        showEmpty: false,
                        top: '80%',
                        height: '20%',
                        offset: 2,
                        lineWidth: 1,
                        opposite: false,
                    }
                ],

                series: [
                    {
                        name: 'Market Cap',
                        data: marketCaps,
                        tooltip: {
                            valueSuffix: ' USD',
                        },
                        dataGrouping: {
                            enabled: false
                        },
                        visible: false,
                    },

                    {
                        name: 'Price (USD)',
                        data: prices,
                        yAxis:1,
                        toltip: {
                            valuePrefix: '$ '
                        },
                        dataGrouping: {
                            enabled: false
                        }
                    },
                    {
                        type: 'column',
                        name: '24h Vol',
                        data: volumes,
                        yAxis: 2,
                        tooltip: {
                            valueSuffix: ' USD',
                        },
                        dataGrouping: {
                            enabled: false
                        }
                    }
                ],

                credits: {
                    href: window.location.href,
                    text: "Coinmarketcap",
                }
            });
        });

        $("#linear").click(function() {
            yAxises = chart.yAxis;
            yAxises.forEach(function(yAxis) {
                yAxis.update({type: 'linear'});
            });
        });

        $("#log").click(function() {
            yAxises = chart.yAxis;
            yAxises.forEach(function(yAxis) {
                yAxis.update({type: 'logarithmic'});
            });
        });
    </script>
@endsection