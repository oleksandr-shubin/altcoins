@extends('layouts.layout')
@section('title', 'Index')

@section('content')
    @include ('layouts.currenciesPaginateTable', ['currencies' => $coins])
@endsection

@section('scripts')
@endsection