@extends('layouts.layout')
@section('title', 'Index')

@section('content')
    @include ('layouts.currenciesPaginateTable', ['currencies' => $tokens])
@endsection

@section('scripts')
@endsection