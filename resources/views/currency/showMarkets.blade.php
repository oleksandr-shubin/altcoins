@extends('layouts.layout')
@section('title', $currency->name)

@section('content')
    @include('layouts.currenciesData', $currency)
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="card1">

                    @include('layouts.currencyMenu', $currency)

                    <!-- Tab panes -->
                        <div class="tab-content">

                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Source</th>
                                    <th scope="col">Pair</th>
                                    <th scope="col">Volume (24h)</th>
                                    <th scope="col">Price</th>
                                    <th scope="col">Volume (%)</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1 ?>
                                @foreach ($currency->marketsSortedByVolumeUsd24hDesc as $market)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>
                                            <a href="/exchange/{{ $market->exchange->id }}">
                                                {{ $market->exchange->name }}
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{ $market->trade_link }}">
                                                {{ $market->pair() }}
                                            </a>
                                        </td>
                                        <td>
                                            @if(!$market->has_fees)
                                                **
                                            @endIf
                                            {{ "$" . number_format($market->volume_usd_24h) }}
                                        </td>
                                        <td>
                                            {{ "$" . number_format($market->price_usd_view, 2) }}
                                        </td>
                                        <td>{{ number_format($market->volumePercent($currency->volume_usd_24h), 2) . "%" }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            ** Volume excluded - no trading fees

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
@endsection