@extends('layouts.layout')
@section('title', 'Monthly volume')

@section('content')
    <section class="ex24">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php $currency_index = 1 ?>
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th style="text-align:left;" scope="col">Name</th>
                            <th scope="col">Symbol</th>
                            <th scope="col">Volume (1d)</th>
                            <th scope="col">Volume (7d)</th>
                            <th scope="col">Volume (30d)</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $market_index = 1 ?>
                        @foreach($cryptoCurrencies as $currency)
                            <tr>
                                <td>{{ $market_index++ }}</td>
                                <td style="text-align:left;">
                                    <img height="32" width="32" src="{{ $currency->icon_link }}">
                                    <a href="/currency/{{ $currency->id }}">{{ $currency->name }}</a>
                                </td>
                                <td>{{ $currency->mainSymbol->value }}</td>
                                <td><a href="/currency/{{ $currency->id }}/markets">{{ "$" . number_format($currency->volumeDay()) }}</a></td>
                                <td><a href="/currency/{{ $currency->id }}/markets">{{ "$" . number_format($currency->volumeWeek()) }}</a></td>
                                <td><a href="/currency/{{ $currency->id }}/markets">{{ "$" . number_format($currency->volumeMonth()) }}</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
@endsection