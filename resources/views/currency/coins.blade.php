@extends('layouts.layout')
@section('title', 'Index')

@section('content')
    @include ('layouts.currenciesFullTable')
@endsection

@section('scripts')
    <script src="/js/coin-filter.js"></script>
@endsection