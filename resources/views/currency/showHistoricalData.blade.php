@extends('layouts.layout')
@section('title', $currency->name)

@section('content')
    @include('layouts.currenciesData', $currency)
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="card1">

                    @include('layouts.currencyMenu', $currency)

                    <!-- Tab panes -->
                        <div class="tab-content">

                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Open</th>
                                    <th>High</th>
                                    <th>Low</th>
                                    <th>Close</th>
                                    <th>Volume</th>
                                    <th>Market Cap</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($currency->historicalDatasSortedByDateDesc() as $historicalData)
                                    <tr>
                                        <td>{{ \Carbon\Carbon::parse($historicalData->date)->format('M d, Y') }}</td>
                                        <td>{{ nullable_number_format($historicalData->open_usd, 2) }}</td>
                                        <td>{{ nullable_number_format($historicalData->high_usd, 2) }}</td>
                                        <td>{{ nullable_number_format($historicalData->low_usd, 2) }}</td>
                                        <td>{{ nullable_number_format($historicalData->close_usd, 2) }}</td>
                                        <td>{{ nullable_number_format($historicalData->volume_usd) }}</td>
                                        <td>{{ nullable_number_format($historicalData->market_cap_usd) }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
@endsection