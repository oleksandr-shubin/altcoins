@extends('layouts.layout')
@section('title', 'Index')

@section('content')
    @include ('layouts.currenciesPaginateTable', ['currencies' => $cryptoCurrencies])
@endsection

@section('scripts')
@endsection