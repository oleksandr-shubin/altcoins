@extends('layouts.layout')
@section('title', 'Volume')

@section('content')
    <section class="ex24">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php $currency_index = 1 ?>
                    @foreach($cryptoCurrencies as $currency)
                        <div class="card_ex">
                            <div class="card_ex_head">
                                <div class="card-ex-num">{{ $currency_index++ }}.</div>
                                <div class="card-ex-name">{{ $currency->name}} ({{ number_format($currency->volumePercent($totalVolumeUsd), 2)}}%)</div>
                            </div>

                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Source</th>
                                        <th scope="col">Pair</th>
                                        <th scope="col">Volume (24h)</th>
                                        <th scope="col">Price</th>
                                        <th scope="col">Volume (%)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $market_index = 1 ?>
                                @foreach ($currency->topMarketsSortedByVolumeUsd24hDesc() as $market)
                                    <tr>
                                        <td>{{ $market_index++ }}</td>
                                        <td>
                                            <a href="/exchange/{{ $market->exchange_id }}">
                                                {{ $market->exchange->name }}
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{ $market->trade_link }}">
                                                {{ $market->pair() }}
                                            </a>
                                        </td>
                                        <td>{{ "$" . number_format($market->volume_usd_24h) }}</td>
                                        <td>{{ "$" . number_format($market->price_usd, 2) }}</td>
                                        <td>{{ number_format($market->volumePercent($currency->volume_usd_24h), 2) . "%" }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
@endsection