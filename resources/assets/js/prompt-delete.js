$(document).ready(function() {
    $('.al-prompt-delete').submit(function(event) {
        var form = this;
        event.preventDefault();
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this entity!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
            if (willDelete) {
                form.submit();
            } else {
                swal("Entity is safe!");
            }
        });
    })
});