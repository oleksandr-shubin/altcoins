<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currencies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rank')->unsigned()->nullable();
            $table->string('name');
            $table->string('icon_link')->nullable();
            $table->boolean('is_fiat')->default(false);
            $table->boolean('is_token')->default(false);
            $table->string('coinmarketcap_id')->unique()->nullable();

            $table->decimal('price_usd', 25, 6)->nullable();
            $table->decimal('price_btc', 16, 8)->nullable();

            $table->decimal('volume_usd_24h',25,6)->nullable();
            $table->decimal('volume_btc_24h',16,8)->nullable();
            $table->string('volume_native_24h')->nullable();

            $table->decimal('market_cap_usd', 25,6)->nullable();

            $table->decimal('available_supply', 16, 1)->nullable();
            $table->decimal('total_supply', 16, 1)->nullable();
            $table->decimal('max_supply', 16, 1)->nullable();

            $table->decimal('burned_supply', 16, 1)->nullable();
            $table->decimal('private_supply', 16, 1)->nullable();


            $table->boolean('has_available_supply_on_coinmarketcap')->nullable();
            $table->string('available_supply_link')->nullable();
            $table->string('available_supply_index')->nullable();

            $table->decimal('percent_change_1h',10,2)->nullable();
            $table->decimal('percent_change_24h',10,2)->nullable();
            $table->decimal('percent_change_7d',10,2)->nullable();
            $table->integer('last_updated_on_coinmarketcap')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('currencies');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
