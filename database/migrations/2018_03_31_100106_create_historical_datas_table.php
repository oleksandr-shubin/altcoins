<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoricalDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historical_datas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('currency_id')->unsigned()->nullable();
            $table->timestamp('date');

            $table->decimal('open_usd', 25,6)->nullable();
            $table->decimal('high_usd', 25,6)->nullable();
            $table->decimal('low_usd', 25,6)->nullable();
            $table->decimal('close_usd', 25,6)->nullable();
            $table->decimal('volume_usd', 25,6)->nullable();
            $table->decimal('market_cap_usd', 25,6)->nullable();

            $table->timestamps();
            $table->foreign('currency_id')->references('id')->on('currencies')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historical_datas');
    }
}
