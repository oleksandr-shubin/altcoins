<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTickersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('currency_id')->unsigned();

            $table->decimal('price_usd', 25, 6)->nullable();
            $table->decimal('price_btc', 16, 8)->nullable();

            $table->decimal('volume_usd_24h',25,6)->nullable();
            $table->decimal('volume_btc_24h',16,8)->nullable();
            $table->string('volume_native_24h')->nullable();

            $table->timestamps();
            $table->foreign('currency_id')->references('id')->on('currencies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickers');
    }
}
