<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('markets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('exchange_id')->unsigned();
            $table->integer('base_id')->unsigned();
            $table->integer('quote_id')->unsigned();
            $table->string('api_link')->nullable();
            $table->string('trade_link')->nullable();

            $table->decimal('price_usd', 25, 6)->nullable();
            $table->decimal('price_btc', 16, 8)->nullable();
            $table->string('price_native')->nullable();

            $table->decimal('volume_usd_24h', 25, 6)->nullable();
            $table->decimal('volume_btc_24h', 16, 8)->nullable();
            $table->string('volume_native_24h')->nullable();

            $table->string('price_index')->nullable();
            $table->string('volume_24h_index')->nullable();
            $table->boolean("has_fees")->nullable();
            $table->timestamps();
            $table->foreign('exchange_id')->references('id')->on('exchanges')->onDelete('cascade');
            $table->foreign('base_id')->references('id')->on('currencies')->onDelete('cascade');
            $table->foreign('quote_id')->references('id')->on('currencies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('markets');
    }
}
