<?php

use Illuminate\Database\Seeder;

class CryptoCurrenciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $exchangesCount = 903 + 584 ;

        for ($i = 0; $i < $exchangesCount; $i++) {
            DB::table('crypto_currencies')->insert([
                'name' => '',
                'symbol' => '',
                'is_token' => false,
            ]);
        }
    }
}
