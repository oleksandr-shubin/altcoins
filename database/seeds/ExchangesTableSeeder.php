<?php

use Illuminate\Database\Seeder;

class ExchangesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $exchangesCount = 189 ;

        for ($i = 0; $i < $exchangesCount; $i++) {
            DB::table('exchanges')->insert([
                'name' => '',
                'api_link' => '',
            ]);
        }
    }
}
