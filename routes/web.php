<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'CurrencyController@indexPaginate');
Route::get('/all', 'CurrencyController@index');
Route::get('/token', 'CurrencyController@indexTokensPaginate');
Route::get('/token/all', 'CurrencyController@indexTokens');
Route::get('/coin', 'CurrencyController@indexCoinsPaginate');
Route::get('/coin/all', 'CurrencyController@indexCoins');


Route::group(['prefix' => 'currency'], function() {
    Route::get('/{currency}', 'CurrencyController@show')->name('currency-chart');
    Route::get('/{currency}/markets', 'CurrencyController@showMarkets')->name('currency-markets');
    Route::get('/{currency}/historical-data', 'CurrencyController@showHistoricalData')->name('currency-historical-data');
    Route::get('/{currency}/get-chart', 'CurrencyController@getChart');
});

Route::get('/volume/currency', 'CurrencyController@indexVolume');
Route::get('/volume/currency/monthly', 'CurrencyController@indexVolumeMonthly');
Route::get('/volume/exchange', 'ExchangeController@index');

Route::group(['prefix' => 'search'], function() {
    Route::get('/exchanges', 'SearchController@showExchanges');
    Route::get('/currencies', 'SearchController@showCurrencies');
});


Route::group(['prefix' => 'exchange'], function() {
    Route::get('/{exchange}', 'ExchangeController@show');
});

// Solely for debug purpose
//Route::group(['prefix' => 'utility'], function() {
//   Route::get('/test', 'UtilityController@test');
//});


$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login', 'Auth\LoginController@login');
$this->post('logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function() {
    Route::get('/', 'AdminController@index');
    Route::delete('/log/regularly', 'AdminController@clearRegularlyLog');
    Route::delete('/log/daily', 'AdminController@clearDailyLog');

    Route::get('/{user}/edit', 'AdminController@edit');
    Route::get('/{user}/edit/password', 'AdminController@editPassword');
    Route::patch('/{user}', 'AdminController@update');
    Route::patch('/{user}/password', 'AdminController@updatePassword');

    Route::get('currency', 'Admin\CurrencyController@index');
    Route::get('currency/create', 'Admin\CurrencyController@create');
    Route::post('currency', 'Admin\CurrencyController@store');
    Route::get('currency/{currency}/edit', 'Admin\CurrencyController@edit');
    Route::post('currency/{currency}', 'Admin\CurrencyController@update');
    Route::delete('currency/{currency}', 'Admin\CurrencyController@destroy');

    Route::group(['prefix' => 'currency/{currency}'], function(){
        Route::get('/links', 'Admin\LinkController@index');
        Route::get('/link/create', 'Admin\LinkController@create');
        Route::post('/link', 'Admin\LinkController@store');
        Route::get('/link/{link}/edit', 'Admin\LinkController@edit');
        Route::patch('/link/{link}', 'Admin\LinkController@update');
        Route::delete('/link/{link}', 'Admin\LinkController@destroy');
    });

    Route::get('exchange', 'Admin\ExchangeController@index');
    Route::get('exchange/create', 'Admin\ExchangeController@create');
    Route::post('exchange', 'Admin\ExchangeController@store');
    Route::get('exchange/{exchange}/edit', 'Admin\ExchangeController@edit');
    Route::patch('exchange/{exchange}', 'Admin\ExchangeController@update');
    Route::delete('exchange/{exchange}', 'Admin\ExchangeController@destroy');

    Route::group(['prefix' => 'exchange/{exchange}'], function(){
        Route::get('/markets', 'Admin\MarketController@index');
        Route::get('/market/create', 'Admin\MarketController@create');
        Route::post('/market', 'Admin\MarketController@store');
        Route::get('/market/{market}/edit', 'Admin\MarketController@edit');
        Route::patch('/market/{market}', 'Admin\MarketController@update');
        Route::delete('/market/{market}', 'Admin\MarketController@destroy');
    });
});

